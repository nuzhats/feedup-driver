//
//  Google.swift
//

import Foundation
import UIKit
import GoogleSignIn
import Foundation
//import Google


protocol GoogleDelegate  {
    func didComplete(data : GoogleVariable)
    func didFailed(error : Error)
    func didCancelled(error : Error)
}

struct GoogleVariable {
    var socialID : String = ""
    var email : String = ""
    var userName : String = ""
    var familyName : String = ""
    var givenName : String = ""
    var image : String = ""
    var msg : String = ""
}

/*enum MyErrors: Int, Error {
    case Unknown = 404
    
    var localizedDescription: String {
        switch self {
        case .Unknown:
            return NSLocalizedString("\(MyErrors.self)_\(self.localizedDescription)", tableName: String(describing: self), bundle: Bundle.main, value: "Cancel Button Tap", comment: "")
        
    }
    var _code: Int { return self.rawValue }
}*/

class Google: UIViewController,GIDSignInDelegate{//,GIDSignInUIDelegate {
    
    var delegate : GoogleDelegate!
    var currentNavigation = UINavigationController()
    private var data = GoogleVariable()
    
    class func configure()
    {
        var configureError: NSError?
        //GGLContext.sharedInstance().configureWithError(&configureError)
        //assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
    }
    
    func LoginWithGoogle(selfView : UIViewController,GOOGLE_CLIENT_ID : String)  {
        
        currentNavigation = selfView.navigationController != nil ? selfView.navigationController! : UINavigationController()
        
        currentNavigation.addChild(self)
        
        let arrayScopes = ["https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/plus.me"]
        //GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes = arrayScopes
        GIDSignIn.sharedInstance().clientID = GOOGLE_CLIENT_ID
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().signIn()
        
        
    }
    
    func LogoutFromGoogle()  {
        GIDSignIn.sharedInstance().signOut()
        
        if delegate != nil
        {
            var data = GoogleVariable()
            data.msg = "Logout Successfully."
            delegate.didComplete(data: data)
        }
//        if GIDSignIn.sharedInstance().hasAuthInKeychain()
//        {
//            GIDSignIn.sharedInstance().signOut()
//
//            if delegate != nil
//            {
//                var data = GoogleVariable()
//                data.msg = "Logout Successfully."
//                delegate.didComplete(data: data)
//            }
//        }
//        else
//        {
//            if delegate != nil
//            {
//                delegate.didFailed(error: MyErrors.Unknown)
//            }
//        }
    }
    
    //MARK: Google Delegate
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!)
    {
        self.dismiss(animated: true, completion: nil)
        
        //        if delegate != nil {
        //            delegate.didComplete(data: data)
        //        }
        //        removeSelfFromNavigation()
    }
    
    open func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error == nil)
        {
            data.socialID = user.userID
            data.email = user.profile.email
            data.familyName = user.profile.familyName
            data.givenName = user.profile.givenName
            data.userName = user.profile.name
            data.image = user.profile.imageURL(withDimension: 200).absoluteString
            
            if delegate != nil {
                delegate.didComplete(data: data)
            }
            
        }
        else
        {
            delegate.didFailed(error: error)
        }
        //self.dismiss(animated: true, completion: nil)
        removeSelfFromNavigation()
    }
    
    open func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!)
    {
        if delegate != nil
        {
            delegate.didFailed(error: error)
        }
        
        removeSelfFromNavigation()
    }
    
    private func removeSelfFromNavigation() {
        
        if currentNavigation.viewControllers.count > 0 {
            
            if(currentNavigation.viewControllers.contains(self))
            {
                currentNavigation.viewControllers.remove(at: currentNavigation.viewControllers.index(of: self)!)
            }
        }
    }
}
