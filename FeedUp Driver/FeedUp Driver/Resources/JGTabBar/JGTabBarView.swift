//
//  JGTabBarView.swift
//  CustomeTabBar
//
//  Created by JayeshGodhaniya on 21/09/21.
//  Copyright © 2021 JayeshGodhaniya. All rights reserved.
//

import UIKit

@IBDesignable class JGTabBarView: UITabBar
{
    @IBInspectable var color: UIColor = .white
    @IBInspectable var radiusFloat: CGFloat = 15.0
    @IBInspectable var shadowColor: UIColor = .white
    @IBInspectable var ShadowOpacityFloat: Float = 0.5
    @IBInspectable var ShadowRadiusFloat: CGFloat = 5.0
    @IBInspectable var FirstGredientColor: UIColor = .white
    @IBInspectable var SecondGredientColor: UIColor = UIColor(displayP3Red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 0.75)
    
    private var shapeLayer: CALayer?

    override func draw(_ rect: CGRect) {
        addShape()
    }

    private func addShape() {
        //Create a CAShapeLayer for Set Rounded Corner tab bar
        let shapeLayer = CAShapeLayer()
        shapeLayer.shadowColor = shadowColor.cgColor
        shapeLayer.shadowOffset = CGSize(width: 0, height: -3.0)
        shapeLayer.shadowOpacity = ShadowOpacityFloat
        shapeLayer.shadowRadius = ShadowRadiusFloat
        shapeLayer.path = createPath()
        shapeLayer.strokeColor = UIColor.gray.withAlphaComponent(0.1).cgColor
        shapeLayer.fillColor = color.cgColor
        shapeLayer.lineWidth = 1
        
        
        if let oldShapeLayer = self.shapeLayer {
            layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        } else {
            layer.insertSublayer(shapeLayer, at: 0)
        }
    
        self.shapeLayer = shapeLayer
        
        //Create a CAGradientLayer for set Gradient Color in tab bar
        let layerGradient = CAGradientLayer()
        layerGradient.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        layerGradient.colors = [FirstGredientColor.cgColor, SecondGredientColor.cgColor]
        layerGradient.cornerRadius = radiusFloat
        //Add sub layer to show Gradient color in tab bar
        self.shapeLayer?.addSublayer(layerGradient)
    }
    
    //Create A Path and show Corner Radius on Top Left and Top Right
    private func createPath() -> CGPath {
        let path = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: [.topLeft, .topRight],
            cornerRadii: CGSize(width: radiusFloat, height: 0.0))
        return path.cgPath
    }
    
}
