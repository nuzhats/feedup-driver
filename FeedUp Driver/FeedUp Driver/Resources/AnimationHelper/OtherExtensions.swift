//
//  OtherExtensions.swift
//  InstaAlerts
//
//  Created by ADMIN on 18/06/19.
//  Copyright © 2019 Rahul Patel. All rights reserved.
//

import UIKit

extension Double
{
    //Return a random floating point number between 0.0 and 1.0, inclusive
    static var random : Double{
        return Double(arc4random())
    }
    
    //Return generated value.
    static func random(min: Double, max: Double) -> Double
    {
        return Double.random * (max - min) + min
    }
}

extension Float
{
    //Return a random floating point number between 0.0 and 1.0, inclusive
    static var random : Float{
        return Float(arc4random())
    }
}

extension CGFloat
{
    //Return a random floating point number between 0.0 and 1.0, inclusive
    static var random : CGFloat{
        
        return CGFloat(arc4random())
    }
    
    //Return generated value.
    static func random(min: CGFloat, max: CGFloat) -> CGFloat
    {
        return CGFloat.random * (max - min) + min
    }
}

