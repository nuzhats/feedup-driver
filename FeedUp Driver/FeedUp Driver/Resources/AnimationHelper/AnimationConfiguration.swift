//
//  AnimationConfiguration.swift
//  InstaAlerts
//
//  Created by ADMIN on 18/06/19.
//  Copyright © 2019 Rahul Patel. All rights reserved.
//

import UIKit

class AnimationConfiguration {
    
    static var offset: CGFloat = 30.0
    
    //Duration of the animation.
    static var duration: Double = 0.35
    
    //Interval for animations handling multiple views that need to be animated one after the other and not at the same time.
    static var interval: Double = 0.075
    
    static var maxZoomScale: Double = 2.0
    
    //Maximum rotation (left or right)
    static var maxRotationAngle: CGFloat = .pi / 4
    
}
