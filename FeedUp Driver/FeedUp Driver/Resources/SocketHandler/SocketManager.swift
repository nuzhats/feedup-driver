//
//  SocketManager.swift
//  FeedUp Driver
//
//  Created by Macbook on 24/09/21.
//

/*import Foundation
import SocketIO

let manager = SocketManager(socketURL: URL(string: SOCKET_URL)!, config:[.log(true), .forceWebsockets(true), .connectParams(["reconnects": true])])
let socket = manager.defaultSocket
var locationTimer : Timer?

protocol newRequest {
    func newRequestArrive()
}


class SocketManager: NSObject
{
    //MARK: Class Variable
    static let sharedInstance = PBSocketManager()
    let SocketTimeOut : Double = 3000
    var socketJoined : Bool = false
    var req_delegate: newRequest!
    
    //MARK: Socket Basic functions
    func connectWithSocket()
    {
        locationTimer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(updateLocation), userInfo: nil, repeats: true)
        
        //Always Remove old handlers before activate(add) new, If you activate(add) multiple times handler then you will get response multiple time
        socket.removeAllHandlers()
        self.activateAllHandlers()
        self.socketJoined = false
        
        //Connect Socket with Server, We can not pass user data like userId etc with connect event so we need join with server after connection establish, we will pass user data with join event. After getting connect success event
        
        socket.connect(timeoutAfter: SocketTimeOut, withHandler: {
            print("timeout")
        })
    }
    
    //To send data to server using socket
    func sendUsingSocket(event:String,data:SocketData...)
    {
        socket.emit(event,with: data)
    }
    
    
    //To Get data from server using socket
    func didReceiveData(event:String,handler:@escaping (_ data:JSON)->Void){
        
        socket.on(event) {(dataArray, socketAck) -> Void in
            
            print("Event Name : \(event) \n Event Data : \(JSON(dataArray))")
            let jsonObj = JSON(dataArray)
            handler(jsonObj)
        }
    }
    
    //To disconnect socket With server
    func disconnectSocket()
    {
        socketJoined = false
        socket.removeAllHandlers()
        manager.forceNew = true
        socket.disconnect()
    }
    
    var locationCount : Int = 0
    
    @objc func updateLocation()
    {
        //createLocalNotification(messageBody: "location updates")
        //kFireLocalNotification(title: "bakwas kishan", text: "location update", userInfo: [:])
        if kCurrentUser.id != ""
        {
            //Update to firebase server every 5 Sec
            print("Current Location : \(String(describing: AppInstance.getCurrentLocation()))")
            
            //Update to backend server every 30 Sec
            //FIXME: UNMARK LOCATION UPDATE CAAL
            callUpdateLocation()
            
        }
    }
    
    func callUpdateLocation()
    {
        
        if let location = AppInstance.getCurrentLocation()
        {
            var currentTripID = "0"
            var distanceToDropOff = "0"
            let nextRide = "0"
            
            if Int(kCurrentUser.ongoing_trip_id) != nil
            {
                currentTripID = kCurrentUser.ongoing_trip_id
            }
            if Double(kCurrentUser.ongoing_dropoff_latitude) != nil && Double(kCurrentUser.ongoing_dropoff_longitude) != nil
            {
                let temploc1 = CLLocation(latitude: location.latitude, longitude: location.longitude)
                let temploc2 = CLLocation(latitude: Double(kCurrentUser.ongoing_dropoff_latitude)!, longitude: Double(kCurrentUser.ongoing_dropoff_longitude)!)
                let tempDist = AppInstance.calculateDistance(coordinate₀: temploc1, coordinate₁: temploc2)
                distanceToDropOff = String(format: "%.0f", tempDist)
                
            }
            
            if socket.status == .connected && PBSocketManager.sharedInstance.socketJoined
            {
                //Emit using socket
                
                PBSocketManager.sharedInstance.sendUsingSocket(event: SocketEvent.SOCKET_UPDATE_LOCATION.rawValue, data: kCurrentUser.id, location.latitude, location.longitude)
                print("location update using socket")
                //fire notification observer
                NotificationCenter.default.post(name: Notification.Name(
                    rawValue: obzerverTypes.update_location.rawValue), object: nil)
            }
            else
            {
                //Emit using API
                SendDriverLocation(latitude: location.latitude.description, longitude: location.longitude.description, response: { (response) in
                    switch response
                    {
                        
                    case .success(let data):
                        print("location data:\(data)")
                        
                        //fire notification observer
                        NotificationCenter.default.post(name: Notification.Name(
                            rawValue: obzerverTypes.update_location.rawValue), object: nil)
                    case .failed(let error):
                        print("error is :\(error)")
                    }
                })
            }
        }
    }

}

extension SocketManager
{
    //MARK: Activate(Add) all required Observer ++++++++++++++++++++++
    
    func activateAllHandlers()
    {
        
        //MARK: Common Observer
        
        //Socket Connect success Observer
        socket.on(clientEvent: .connect) {data, ack in
            
            //Socket Successfully Connected, Now Join USer with Server
            if socket.status == .connected //&& PBSocketManager.sharedInstance.socketJoined
            {
                if kCurrentUser.id != ""
                {
                    //Send data using Socket
                    let parameters: [String: Any] = ["id": kCurrentUser.id]
                    
                    PBSocketManager.sharedInstance.sendUsingSocket(event: SocketEvent.SOCKET_JOIN.rawValue, data: parameters)
                    
                    
                }
                
                //Data Send To server, You will get response in handler
            }
        }

        // Socket Status Change
        socket.on(clientEvent: .statusChange){data, ack in
            print("status change:\(socket.status)")
            if socket.status != .connected
            {
                self.socketJoined = false
            }
        }
        
        //Socket Error Observer
        socket.on(clientEvent: .error) {data, ack in
            print("socket error")
            print(data.debugDescription)
            self.socketJoined = false
            
            if kCurrentUser.id != ""
            {
                self.callUpdateLocation()
            }
            
        }
        
        //Socket Disconnect Observer
        socket.on(clientEvent: .disconnect) {data, ack in
            print("socket Disocnnect successfully")
            self.socketJoined = false
        }
        
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_JOIN.rawValue, handler: {(data) in
            
            print("Socket Succesfully Joined")
            self.socketJoined = true
        })
        
        //Socket Error in backend Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_ERROR.rawValue, handler: {(data) in
            
            print("Backend Error : \n \(data) \n")
            
        })
        
        //MARK: - Socket on reject Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_REJECTT.rawValue, handler: {(data) in
            
            print("on reject")
            
            if data.count>0
            {
                print(data)
                clearTripData()
            }
            
        })
        
        //MARK: - Socket on accept Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_ACCEPT.rawValue, handler: {(data) in
            print("on accept")
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    
                    requestAccept(data: json["trip_details"])
                }
            }
        })
        
        //MARK: - Socket new request Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_NEW_REQUEST.rawValue, handler: {(data) in
            print("new request")
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    
                    getNewRideRequest(data: json)
                    
                    //self.req_delegate.newRequestArrive()
                }
            }
        })
        
        //MARK: - Socket user cancel Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_USER_CANCEL.rawValue, handler: {(data) in
            print("user cancel")
            
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    
                    if ((AppInstance.window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).visibleViewController?.classForCoder == RequestViewController.classForCoder()
                    {
                        let vc = ((AppInstance.window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).visibleViewController
                        vc?.dismiss(animated: true, completion: nil)
                        
                    }
                    else
                    {
                        let _msg = rideCancelMsg + " " + kCurrentUser.ongoing_user_name
                        createLocalNotification(messageBody: _msg)
                        AppInstance.showAlert(title: rideCancelled, msg: _msg, btn: OK, type: "cancel_ride")
                    }
                    
                }
            }
            
        })
        
        
        //MARK: - Socket on driver cancel Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_DRIVER_CANCEL.rawValue, handler: {(data) in
            print("on driver cancel")
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    clearTripData()
                    AppInstance.goHomePage(transition: true)
                }
            }
            
        })
        
        //MARK: - Socket on start trip Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_START_TRIP.rawValue, handler: {(data) in
            print("on start trip")
            if data.count>0
            {
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    let trip = json["trip_details"]
                    updateStatusOnView(code: StringValue(value : trip["trip_status"]), awayTime: StringValue(value : trip["user_away_time"]))
                    
                    //fire notification observer for accept status
                    NotificationCenter.default.post(name: Notification.Name(
                        rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
                }
            }
        })
        
        
        //MARK: - Socket on complete trip Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_COMPLETE_TRIP.rawValue, handler: {(data) in
            print("on complete trip")
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    
                    NotificationCenter.default.post(name: Notification.Name(
                        rawValue: obzerverTypes.complete_request.rawValue), object: nil, userInfo: ["currency": json["payments_details"]["currency"].description, "baseFare" : json["payments_details"]["base_fare"].description, "waitingcharges" : json["payments_details"]["waiting_charge"].description, "extraMiles" : json["payments_details"]["extra_miles"].description, "total" : json["payments_details"]["total"].description, "is_free" : json["payments_details"]["is_free"].description, "vat_amount" : json["payments_details"]["vat_amount"].description, "vat_percent" : json["payments_details"]["vat_percent"].description])
                }
            }
        })
        
        //MARK: - Socket on update status Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_UPDATE_STATUS.rawValue, handler: {(data) in
            print("on update status")
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    //StringValue(value : json["trip_details"]["trip_status"])
                    print("on pdate status:\(StringValue(value : json["trip_details"]["trip_status"]))")
                    updateStatusOnView(code: StringValue(value : json["trip_details"]["trip_status"]), awayTime: "")
                    //fire notification observer for accept status
                    NotificationCenter.default.post(name: Notification.Name(
                        rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
                }
                
            }
        })
        
        //MARK: - Socket service request Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_SERVICE_REQUEST.rawValue, handler: {(data) in
            print("service request")
        })
        
        //MARK: - Socket new service request Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_NEW_SERVICE_REQUEST.rawValue, handler: {(data) in
            print("new service request")
            print(data)
            if data.count>0
            {
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    getNewServiceRequest(data: json)
                }
            }
        })
        
        //MARK: - Socket on accept service Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_ACCEPT_SERVICE.rawValue, handler: {(data) in
            print("on accept service")
            print(data)
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    
                    serviceAccept(data: json["service_details"])
                }
            }
        })
        
        //MARK: - Socket on reject service Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_REJECT_SERVICE.rawValue, handler: {(data) in
            print(data)
            if data.count>0
            {
                //fire notification observer for accept status
                NotificationCenter.default.post(name: Notification.Name(
                    rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
                
            }
        })
        
        //MARK: - Socket on start service Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_START_SERVICE.rawValue, handler: {(data) in
            print("on start service")
            if data.count>0
            {
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    let trip = json["service_details"]
                    updateStatusOnView(code: StringValue(value : trip["service_status"]), awayTime: StringValue(value : trip["user_away_time"]))
                    
                    //fire notification observer for accept status
                    NotificationCenter.default.post(name: Notification.Name(
                        rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
                }
            }
            
        })
        
        //MARK: - Socket on complete service Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_COMPLETE_SERVICE.rawValue, handler: {(data) in
            print("on complete service")

            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    
                    var services = [ServiceDataDetails]()
                    if json["payments_details"]["services"] != JSON.null
                    {
                        services.removeAll()
                        services.append(contentsOf: ServiceDataDetails.addService(array: json["payments_details"]["services"].arrayValue))
                    }
                    NotificationCenter.default.post(name: Notification.Name(
                        rawValue: obzerverTypes.complete_request.rawValue), object: nil, userInfo: ["currency": services[0].currency, "baseFare" : json["base_fare"].description, "waitingcharges" : json["waiting_charge"].description, "extraMiles" : json["extra_miles"].description, "total" : json["total"].description, "services": services, "vat_amount" : json["vat_amount"].description, "vat_percent" : json["vat_percent"].description])
                }
            }
        })
        
        //MARK: - Socket on complete payment Observer
        PBSocketManager.sharedInstance.didReceiveData(event: SocketEvent.SOCKET_ON_CONFIRM_PAYMENT.rawValue, handler: {(data) in
            print("on complete payment")
            if data.count>0
            {
                print(data)
                if let jsonString = data[0].string
                {
                    let getTripObj = self.convertToDictionary(text: jsonString)
                    let json = JSON(getTripObj!)
                    print("json = \(json)")
                    
                    clearTripData()
                    AppInstance.goHomePage(transition: true)
                }
            }
        })
       
    }
    
    func getNewRideRequest(data : JSON)
    {
        if kCurrentUser.ongoing_trip_id == "0"
        {
            let message = "New ride request"
            createLocalNotification(messageBody: message)
            let trip = data["trip_details"]
            let driver = data["for_driver"]
            
            kCurrentUser.ongoing_pickup_latitude = StringValue(value : driver["pickup_latitude"])
            
            kCurrentUser.ongoing_user_id = StringValue(value : driver["user_id"])
            
            kCurrentUser.ongoing_pickup_location = StringValue(value : driver["pickup_location"])
            
            kCurrentUser.ongoing_user_name = StringValue(value : driver["user_name"])
            
            kCurrentUser.ongoing_user_image = StringValue(value : driver["user_image"])
            
            kCurrentUser.ongoing_pickup_longitude = StringValue(value : driver["pickup_longitude"])
            
            kCurrentUser.ongoing_user_contact_no = StringValue(value : driver["user_contact_number"])
            
            kCurrentUser.ongoing_trip_id = StringValue(value : driver["trip_id"])
            
            kCurrentUser.ongoing_trip_type = StringValue(value : driver["trip_type"])
            
            kCurrentUser.ongoing_dropoff_latitude = StringValue(value : driver["dropoff_latitude"])
            
            kCurrentUser.ongoing_dropoff_longitude = StringValue(value : driver["dropoff_longitude"])
            
            kCurrentUser.ongoing_dropoff_location = StringValue(value : driver["dropoff_location"])
        
            kCurrentUser.ongoing_timestamp = Date().timeIntervalSince1970.description
            
            kCurrentUser.saveToDefault()
            
            
            //fire notification observer
            NotificationCenter.default.post(name: Notification.Name(
                rawValue: obzerverTypes.new_request.rawValue), object: nil)
        }
    }

    func getNewServiceRequest(data : JSON)
    {
        if kCurrentUser.ongoing_trip_id == "0"
        {
            let message = "New service request"
            createLocalNotification(messageBody: message)
            let trip = data["service_details"]
            let driver = data["for_driver"]
            kCurrentUser.ongoing_pickup_latitude = StringValue(value : trip["provider_details"]["latitude"])
            
            kCurrentUser.ongoing_user_id = StringValue(value : trip["provider_details"]["provider_id"])
            
            
            kCurrentUser.ongoing_pickup_location = StringValue(value : trip["provider_details"]["location"])
            
            kCurrentUser.ongoing_user_name = StringValue(value : trip["provider_details"]["provider_name"])
            
            kCurrentUser.ongoing_user_image = StringValue(value : trip["user_image"])
            
            kCurrentUser.ongoing_trip_id = StringValue(value : data["booking_id"])
            
            kCurrentUser.ongoing_pickup_longitude = StringValue(value : trip["provider_details"]["longitude"])
            
            kCurrentUser.ongoing_user_contact_no = StringValue(value : trip["provider_details"]["provider_contact_no"])
            
            kCurrentUser.ongoing_payment_method = StringValue(value : trip["payment_method"])
            
            kCurrentUser.vehicle_color = StringValue(value : driver["vehicle_color"])
            kCurrentUser.vehicle_number_plate = StringValue(value : driver["vehicle_number"])
            kCurrentUser.vehicle_brand = StringValue(value : driver["vehicle_brand"])
            kCurrentUser.vehicle_model = StringValue(value : driver["vehicle_model"])
            
            var services = [ServiceDataDetails]()
            services.removeAll()
            
            if trip["services"] != JSON.null
            {
                services.append(contentsOf: ServiceDataDetails.addService(array: trip["services"].arrayValue))
                
                let count = services.count
                for i in 0..<count
                {
                    if i == 0
                    {
                        kCurrentUser.services = services[i].service_name
                    }
                    else
                    {
                        kCurrentUser.services = kCurrentUser.services + "," + " " + services[i].service_name
                    }
                }
            }
            
            kCurrentUser.ongoing_timestamp = Date().timeIntervalSince1970.description
            
            kCurrentUser.saveToDefault()
            
            //fire notification observer
            NotificationCenter.default.post(name: Notification.Name(
                rawValue: obzerverTypes.new_request.rawValue), object: nil)
            
        }
    }

    func requestAccept(data : JSON)
    {
        if kCurrentUser.ongoing_trip_id != "0"
        {
            kCurrentUser.ongoing_dropoff_latitude = StringValue(value : data["dropoff_latitude"])
            
            kCurrentUser.ongoing_dropoff_longitude = StringValue(value : data["dropoff_longitude"])
            
            kCurrentUser.ongoing_dropoff_location = StringValue(value : data["dropoff_location"])
            kCurrentUser.saveToDefault()
            updateStatusOnView(code: StringValue(value : data["trip_status"]), awayTime: StringValue(value : data["user_away_time"]))
            
            //fire notification observer for accept status
            NotificationCenter.default.post(name: Notification.Name(
                rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
            
            //fire notification observer for arrived status
            NotificationCenter.default.post(name: Notification.Name(
                rawValue: obzerverTypes.update_status.rawValue), object: nil)
        }
    }

    func serviceAccept(data : JSON)
    {
        if kCurrentUser.ongoing_trip_id != "0"
        {
            updateStatusOnView(code: StringValue(value : data["service_status"]), awayTime: StringValue(value : data["user_away_time"]))
            
            //fire notification observer for accept status
            NotificationCenter.default.post(name: Notification.Name(
                rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
            
            //fire notification observer for arrived status
            NotificationCenter.default.post(name: Notification.Name(
                rawValue: obzerverTypes.update_status.rawValue), object: nil)
        }
    }

    func clearTripData()
    {
        //ongoing trip details
        kCurrentUser.ongoing_timestamp = ""
        kCurrentUser.ongoing_trip_id = "0"
        kCurrentUser.ongoing_status = ""
        kCurrentUser.ongoing_status_code = ""
        kCurrentUser.ongoing_user_id = ""
        kCurrentUser.ongoing_user_name = ""
        kCurrentUser.ongoing_user_image = ""
        kCurrentUser.ongoing_map_image = ""
        kCurrentUser.ongoing_pickup_location = ""
        kCurrentUser.ongoing_dropoff_location = ""
        kCurrentUser.ongoing_pickup_latitude = "0"
        kCurrentUser.ongoing_pickup_longitude = "0"
        kCurrentUser.ongoing_dropoff_latitude = "0"
        kCurrentUser.ongoing_dropoff_longitude = "0"
        kCurrentUser.ongoing_payment_method = ""
        kCurrentUser.ongoing_user_contact_no = ""
        kCurrentUser.user_away_time = ""
        kCurrentUser.is_ongoing_trip = ""
        
        kCurrentUser.saveToDefault()
    }

    func updateStatusOnView(code: String, awayTime: String)
    {
        if (awayTime != "")
        {
            kCurrentUser.user_away_time = (Int(awayTime)!/60).description
        }
        
        kCurrentUser.ongoing_status_code = code
        kCurrentUser.ongoing_status = Utilities.checkStatus(code: code)
        
        kCurrentUser.saveToDefault()
    }

    func calculateDistance() -> Int
    {
        if let location = AppInstance.getCurrentLocation()
        {
            if kCurrentUser.ongoing_status_code == "6"
            {
                let coordinate0 = CLLocation(latitude: location.latitude, longitude: location.longitude)
                let coordinate1 = CLLocation(latitude: Double(kCurrentUser.ongoing_dropoff_latitude)!, longitude: Double(kCurrentUser.ongoing_dropoff_longitude)!)
                
                let distanceInMeters = coordinate0.distance(from: coordinate1) // result is in meters
                
                return Int(distanceInMeters)
            }
            else
            {
                let coordinate0 = CLLocation(latitude: location.latitude, longitude: location.longitude)
                let coordinate1 = CLLocation(latitude: Double(kCurrentUser.ongoing_pickup_latitude)!, longitude: Double(kCurrentUser.ongoing_pickup_longitude)!)
                
                let distanceInMeters = coordinate0.distance(from: coordinate1) // result is in meters
                
                return Int(distanceInMeters)
            }
            
        }
        else
        {
            return 0
        }
    }

    func StringValue(value : JSON) -> String
    {
        if value != JSON.null
        {
            return value.description
        }
        else
        {
            return ""
        }
    }


    extension PBSocketManager //Send Data
    {
        func convertToDictionary(text: String) -> [String: Any]? {
            if let data = text.data(using: .utf8) {
                do
                {
                    return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                } catch {
                    print(error.localizedDescription)
                }
            }
            return nil
        }
        
        //MARK: Send data To server ++++++++++++++++++++++
        
        func sendRequest(parameters : [String : Any], event: SocketEvent.RawValue, apiAction: String)//(param1 : Any, param2 : Any, param3 : Any)
        {
            /*var parameters = [String : Any]()
            parameters["key1"]  = param1
            parameters["key2"]  = param2
            parameters["key3"]  = param3*/
            
            if socket.status == .connected && PBSocketManager.sharedInstance.socketJoined
            {
               //Send data using Socket
               let jsonString = getJSONstring(value : parameters)
                print(JSON(jsonString))
                PBSocketManager.sharedInstance.sendUsingSocket(event: event, data: apiAuthHeader.token(),jsonString,baseURL)
                //PBSocketManager.sharedInstance.sendUsingSocket(event: event, data: jsonString)
               //Data Send To server, You will get response in handler
            }
            else
            {
                AlamofireModel.alamofireMethod(.post, apiAction: APIAction(rawValue: apiAction)!, parameters: parameters, Header: [:], handler: { (res) in
                    if res.code == 1
                    {
                        let json = JSON(res.Object)
                        print(json)
                    }
                    else
                    {
                        print(res.message)
                    }
                }, errorhandler: { (error) in
                   print(error.localizedDescription)
                })
            }
        }
    }

    //MARK:Data Bus +++++++++++++++++++++++++++++++
        
    //Handle Data pass to view controller from common function, YOu can get data from SOCKET, API Response or Push Notification So call this common Function  From All this source
        
    //You can pass data to view Controller from Here using Notification Center post method and observer

    /* Demo Code for ViewController
         
        func setObjerver()
        {
            //Objerver for request sent success
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
         
         
            NotificationCenter.default.addObserver(self, selector: #selector(rideAcceptSuccess(notification:)), name: NSNotification.Name(rawValue: obzerverTypes.request_accepted.rawValue), object: nil)
         
         
         }
         
         @objc func rideAcceptSuccess(notification : NSNotification)
         {
            if let info = notification.userInfo as? [String : Any]
            {
                if let data = info["data"]
                {
         
                     let json = data as JSON
                     print(json)
                }
            }
       */


        
    func requestAcceptByDriver(data : JSON)
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: obzerverTypes.request_accepted.rawValue), object: nil, userInfo : ["data":data])
    }
        
    //MARK: App related socket Events +++++++++++++++++++++++++++++++
    enum SocketEvent : String
    {
        //Changes according your requirement
        case SOCKET_JOIN = "join"
        case SOCKET_ON_JOIN = "onJoin"
        case SOCKET_ON_ERROR = "onError"
        case SOCKET_UPDATE_LOCATION = "updateLocation"
        case SOCKET_ACCEPT_REQUEST = "acceptRequest"
        case SOCKET_ON_REJECTT = "onReject"
        case SOCKET_ON_ACCEPT = "onAccept"
        case SOCKET_NEW_REQUEST = "newRequest"
        case SOCKET_USER_CANCEL = "userCancel"
        case SOCKET_DRIVER_CANCEL = "driverCancel"
        case SOCKET_ON_DRIVER_CANCEL = "onDriverCancel"
        case SOCKET_START_TRIP = "startTrip"
        case SOCKET_ON_START_TRIP = "onStartTrip"
        case SOCKET_COMPLETE_TRIP = "completeTrip"
        case SOCKET_ON_COMPLETE_TRIP = "onCompleteTrip"
        case SOCKET_UPDATE_STATUS = "updateStatus"
        case SOCKET_ON_UPDATE_STATUS = "onUpdateStatus"
        case SOCKET_SERVICE_REQUEST = "serviceRequest"
        case SOCKET_NEW_SERVICE_REQUEST = "newServiceRequest"
        case SOCKET_ACCEPT_SERVICE = "acceptService"
        case SOCKET_ON_ACCEPT_SERVICE = "onAcceptService"
        case SOCKET_ON_REJECT_SERVICE = "onRejectService"
        case SOCKET_START_SERVICE = "startService"
        case SOCKET_ON_START_SERVICE = "onStartService"
        case SOCKET_COMPLETE_SERVICE = "completeService"
        case SOCKET_ON_COMPLETE_SERVICE = "onCompleteService"
        case SOCKET_CONFIRM_PAYMENT = "confirmPayment"
        case SOCKET_ON_CONFIRM_PAYMENT = "onConfirmPayment"
    }

    //MARK: Notification Center obzerverTypes used for Data Bus +++++++++++++++++++++++++++++++
    enum obzerverTypes : String
    {
        case select_brand = "select_brand"
        case new_request = "new_request"
        case complete_request = "complete_request"
        case service_request = "service_request"
        case request_accepted = "request_accepted"
        case request_rejected = "request_rejected"
        case update_location = "update_location"
        case update_status = "update_status"
    }
        
    //MARK: Other Usefull function
    func getJSONstring(value : [String : Any]) ->  NSString
    {
        do {
            let temp = try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted)
        
            return NSString(data: temp, encoding: String.Encoding.ascii.rawValue)!
        }
        catch let error as NSError
        {
            print(error)
            return NSString()
        }
    }

    func createLocalNotification(messageBody:String)
    {
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.body = messageBody
            content.sound = UNNotificationSound.default()
            let notifTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "Alert", content: content, trigger: notifTrigger)
            let center = UNUserNotificationCenter.current()
            center.add(request) { (error : Error?) in
                if let theError = error {
                    print(theError.localizedDescription)
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }

    /*func kFireLocalNotification(title : String, text : String, userInfo : [String : String])
    {
        let notification = UILocalNotification()
        notification.fireDate = Date()
        notification.alertTitle = title
        notification.alertBody = text
        notification.userInfo = userInfo
        
        UIApplication.shared.scheduleLocalNotification(notification)
    }*/
}


*/
