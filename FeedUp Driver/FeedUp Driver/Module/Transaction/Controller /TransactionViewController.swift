//
//  TransactionViewController.swift
//  FeedUp Driver
//
//  Created by Macbook on 24/09/21.
//

import UIKit

class TransactionViewController: UIViewController {

    //MARK: - OUTLETS -
    @IBOutlet weak var btnDebit: UIButton!
    @IBOutlet weak var tableViewTransaction: UITableView!
    
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnCredit: UIButton!
    @IBOutlet weak var lblMinWithrawLimit: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    
    //MARK: - VARIABLES -
    
    
    
    //MARK: - LIFE CYCLE METHODS -
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK: - BUTTON ACTIONS -
    @IBAction func btnCredit(_ sender: Any) {
    }
    
    @IBAction func btnDebit(_ sender: Any) {
    }
    
    @IBAction func btnAll(_ sender: Any) {
    }
    
    @IBAction func btnNotification(_ sender: Any) {
    }
    
    @IBAction func btnWithdraw(_ sender: Any)
    {
        let vcPopUP: WithdrawPopUpVC = Utilities.viewController(name: "WithdrawPopUpVC", onStoryBoared: StoryBoaredNames.transaction.rawValue) as! WithdrawPopUpVC
        vcPopUP.modalPresentationStyle = .overFullScreen
        self.present(vcPopUP, animated: true, completion: nil)
    }
    
}

//MARK: - TABLEVIEW DELEGATE/DATASOURCE -
extension TransactionViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //let cell: TransactionTableViewCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TransactionTableViewCell") as! TransactionTableViewCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
        
        cell.selectionStyle = .none
        return cell
    }
}
