//
//  WithdrawPopUpVC.swift
//  FeedUp Driver
//
//  Created by Jayesh Godhaniya on 29/09/21.
//

import UIKit

class WithdrawPopUpVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtAccountNumber: UITextField!
    @IBOutlet var txtPin: UITextField!
    @IBOutlet var txtAmount: UITextField!
    

    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Button Action Methods --
    @IBAction func btnClose(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnWithdraw(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    

}
//MARK:- UITextField Delegate --
extension WithdrawPopUpVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtName
        {
            txtAccountNumber.becomeFirstResponder()
        }
        else if textField == txtAccountNumber
        {
            txtPin.becomeFirstResponder()
        }
        else if textField == txtPin
        {
            txtAmount.becomeFirstResponder()
        }
        else if textField == txtAmount
        {
            //Withraw api call --
            self.view.endEditing(true)
        }
        else
        {
            self.view.endEditing(true)
        }
        return true
    }
}
