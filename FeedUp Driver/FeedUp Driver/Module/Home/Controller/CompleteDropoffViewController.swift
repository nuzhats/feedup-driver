//
//  CompleteDropoffViewController.swift
//  FeedUp Driver
//
//  Created by Macbook on 29/09/21.
//

import UIKit

class CompleteDropoffViewController: UIViewController {

    //MARK: - OUTLETS -
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgDriver: UIImageView!
    //MARK: - VARIABLES -
    
    //MARK: - LIFE CYCLE METHODS -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK: - BUTTON ACTIONS -

    @IBAction func btnUploadImage(_ sender: Any) {
    }
    
    @IBAction func btnContinue(_ sender: Any)
    {
        //show pp\opup
        
        
        let vc = Utilities.viewController(name: "CustomeSuccessPopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomeSuccessPopUpVC
        vc.popup = popUp.COMPLETE_DROPOFF
        
        
        vc.completionHandler = {
            (type: Int) -> () in
            
            
            if type == 0//Close button
            {
                // perform close action
                vc.dismiss(animated: true, completion: nil)
            }
            else
            {
                //Perform Main action
                vc.dismiss(animated: true, completion: {
                    //go to home
                    
                    AppInstance.gotoHome(transition: true)
                   
                })
                
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    
    }
    
    @IBAction func btnNotification(_ sender: Any) {
    }
    
    //MARK: - API CALLING -
    
    //MARK: - OTHER FUNCTIONS -
    
}
