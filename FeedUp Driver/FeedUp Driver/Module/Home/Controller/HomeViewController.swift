//
//  HomeViewController.swift
//  FeedUp Driver
//
//  Created by Macbook on 24/09/21.
//

import UIKit
import GoogleMaps
import CoreLocation

class HomeViewController: UIViewController {

    //MARK: - OUTLETS -
    @IBOutlet weak var viewOnline: UIView!
    @IBOutlet weak var btnOnline: UIButton!
    @IBOutlet weak var lblOnline: UILabel!
    //Request popup
    @IBOutlet weak var viewRequest: UIView!
    
    @IBOutlet weak var lblRequestStatus: UILabel!
    @IBOutlet weak var btnReqStatus: UIButton!
    @IBOutlet weak var lblRequestDistance: UILabel!
    @IBOutlet weak var lblRequestPrice: UILabel!
    @IBOutlet weak var lblRequestUserName: UILabel!
    @IBOutlet weak var imgRequestUser: UIImageView!
    @IBOutlet weak var lblReqPickUp: UILabel!
    @IBOutlet weak var lblReqDropoff: UILabel!
    
    @IBOutlet weak var btnReqDecline: UIButton!
    @IBOutlet weak var btnReqAccept: UIButton!
    
    
    //pickup/drop off
    
    @IBOutlet weak var heightPicDropAddress: NSLayoutConstraint!//80
    @IBOutlet weak var bottomConstrain: NSLayoutConstraint!//412 - 205
    @IBOutlet weak var viewPickDrop: UIView!
    
    @IBOutlet weak var lblRestaturantName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgRestaurant: UIImageView!
    
    
    @IBOutlet weak var lblRestaurantAddress: UILabel!
    @IBOutlet weak var lblTitlePickup: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    //pickup/dropoff details
    
    @IBOutlet weak var imgRestaurantDetails: UIImageView!
    
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var lblRestaurantAddressDetails: UILabel!
    
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var lblRestaurantNameDetails: UILabel!
    
    @IBOutlet weak var btnPickDrop: UIButton!
    
    //@IBOutlet weak var viewPickDropDetails: UIView!
    
    //Offline popup
    
    @IBOutlet weak var viewOfflone: UIView!
    
    //Map View
    @IBOutlet var mapView: GMSMapView!
    
    //MARK:- VARIABLES -
    var latitude = Double()
    var longitude = Double()
    var isPickUp: Bool = true
    var isExpand: Bool = false
    
    
    //MARK: - LIFE CYCLE METHODS -
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addGestureAnimation()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GetLocation()
        //self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- Get Current Location
    func GetLocation()
    {
        LocationManager.sharedInstance.getLocation(completionHandler: { (location, error) in
            
            if location != nil
            {
                self.latitude = location!.coordinate.latitude
                self.longitude = location!.coordinate.longitude
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                marker.icon = #imageLiteral(resourceName: "Driver location")
                marker.title = ""
                marker.snippet = ""
                
                marker.map = self.mapView
                self.ZoomOnLatLong(lat: self.latitude, Long: self.longitude, Zoom: 7.0)
            }
            else if error != nil
            {
                
            }
        })
    }
    
    //MARK:- Zoom on Lat Long
    func ZoomOnLatLong(lat: Double, Long: Double, Zoom: Float)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(01), execute: {
           let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: Long, zoom: Zoom)

           self.mapView?.animate(to: camera)
       })
    }

    //MARK: - BUTTON ACTIONS -

    @IBAction func btnOnline(_ sender: Any)
    {
        self.manageOnlineOffline(isOffline: true)
    }
    
    //Request popup
    @IBAction func btnReqStatus(_ sender: Any) {
    }
    
    @IBAction func btnReqAccept(_ sender: Any)
    {
        self.manageReqAccept()
    }
    
    @IBAction func btnRestaurantDetails(_ sender: Any)
    {
        let resultVC: OrdersViewController = Utilities.viewController(name: "OrdersViewController", onStoryBoared: StoryBoaredNames.order.rawValue) as! OrdersViewController
        //self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(resultVC, animated: true)
    }
    @IBAction func btnReqDecline(_ sender: Any)
    {
        self.manageReqAccept(isDecline: true)
    }
    
    //pickup/dropoff details
    
    @IBAction func btnMessage(_ sender: Any) {
    }
    
    
    @IBAction func btnCall(_ sender: Any) {
    }
    
    
    @IBAction func btnPickDrop(_ sender: Any)
    {
        
        if isPickUp
        {
            isPickUp = !isPickUp
            managePickDropView(isDropoff: true)
            //managePickDropView()
        }
        else
        {
            //redirect to next screen
            let resultVC: CompleteDropoffViewController = Utilities.viewController(name: "CompleteDropoffViewController", onStoryBoared: StoryBoaredNames.home.rawValue) as! CompleteDropoffViewController
            
            self.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(resultVC, animated: true)
            //managePickDropView(isDropoff: true)
        }
    }
    
    //offline popup
    @IBAction func btnGoOnline(_ sender: Any)
    {
        self.manageOnlineOffline()
        //self.viewOfflone.isHidden = true
    }
    
    //MARK: - OTHER FUNCTIONS -
    func hideShowReqPopup(isHide: Bool = false)
    {
        self.viewRequest.isHidden = isHide
    }
    
    func manageOnlineOffline(isOffline: Bool = false)
    {
        self.viewOnline.isHidden = !isOffline
        self.viewOfflone.isHidden = !isOffline
        
        //FIXME: - manage as per need
        self.viewRequest.isHidden = false
        self.viewPickDrop.isHidden = true
        //self.viewPickDropDetails.isHidden = true
        
    }
    
    func manageReqAccept(isDecline: Bool = false)
    {
        self.viewRequest.isHidden = false
        self.viewOnline.isHidden = true
        self.viewPickDrop.isHidden = true
        //self.viewPickDropDetails.isHidden = true
        
        if isDecline
        {
            self.viewOnline.isHidden = false
            self.viewRequest.isHidden = true
            self.viewPickDrop.isHidden = true
            //self.viewPickDropDetails.isHidden = true
        }
        else
        {
            //show req cpnfirm popup
            
            let vc = Utilities.viewController(name: "CustomeSuccessPopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomeSuccessPopUpVC
            vc.popup = popUp.PICK_UP
            
            
            vc.completionHandler = {
                (type: Int) -> () in
                
                
                if type == 0//Close button
                {
                    // perform close action
                    vc.dismiss(animated: true, completion: nil)
                }
                else
                {
                    //Perform Main action
                    vc.dismiss(animated: true, completion: {
                        //show pickup view
                        self.managePickDropView()
                    })
                    
                }
                
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func managePickDropView(isDropoff: Bool = false)
    {
        self.viewRequest.isHidden = true
        self.viewOnline.isHidden = true
        self.viewPickDrop.isHidden = false
        //self.viewPickDropDetails.isHidden = true
        
        if isExpand
        {
            if isDropoff
            {
                self.btnPickDrop.setTitle(DROPOFF, for: .normal)
            }
            else
            {
                self.btnPickDrop.setTitle(PICKUP, for: .normal)
            }
            
        }
        else
        {
            if isDropoff
            {
                self.lblTitlePickup.text = DROPOFF
            }
            else
            {
                self.lblTitlePickup.text = PICKUP
            }
        }
        
    }
    
    /*func managePickDropDetailView(isDropoff: Bool = false)
    {
        self.viewRequest.isHidden = true
        self.viewOnline.isHidden = true
        self.viewPickDrop.isHidden = true
        self.viewPickDropDetails.isHidden = false
        
        if isDropoff
        {
            
        }
        else
        {
            
        }
    }*/
    
    
    
    //MARK: - Gesture Animation
    func addGestureAnimation()
    {
        let upSwipeGes = UISwipeGestureRecognizer()
        upSwipeGes.direction = .up
        upSwipeGes.addTarget(self, action: #selector(self.swipeUpHandle))
        let downSwipeGes = UISwipeGestureRecognizer()
        downSwipeGes.direction = .down
        downSwipeGes.addTarget(self, action: #selector(self.swipeDownHandle))
        
        
        //self.viewPickDrop.addGestureRecognizer(downSwipeGes)
        self.viewPickDrop.addGestureRecognizer(upSwipeGes)
        
        self.viewPickDrop.addGestureRecognizer(downSwipeGes)
        //self.viewPickDropDetails.addGestureRecognizer(upSwipeGes)
        
    }
    
    @objc func swipeUpHandle()
    {
        self.isExpand = true
        //AppInstance.isHide = true
        UIView.animate(withDuration: 1.0, animations:
            {
                self.imgRestaurantDetails.isHidden = false
                self.bottomConstrain.constant = 0
                self.lblTitlePickup.text = ""
                self.lblPrice.text = ""
                self.heightPicDropAddress.constant = 0
                self.view.layoutIfNeeded()
//                DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
//                    self.viewPickDropDetails.isHidden = false
//                    self.viewPickDrop.isHidden = true
//
//                    self.view.layoutIfNeeded()
//                }
                
        })
        
    }
    
    
    @objc func swipeDownHandle()
    {
        self.isExpand = false
        //AppInstance.isHide = false
        UIView.animate(withDuration: 1.0, animations:
            {
                self.imgRestaurantDetails.isHidden = false
                self.lblTitlePickup.text = PICKUP
                self.lblPrice.text = "$13"
                self.heightPicDropAddress.constant = 80
                self.bottomConstrain.constant = -390
                
//                self.viewPickDropDetails.isHidden = true
//                self.viewPickDrop.isHidden = false
                
                self.view.layoutIfNeeded()
        })
    }
}
