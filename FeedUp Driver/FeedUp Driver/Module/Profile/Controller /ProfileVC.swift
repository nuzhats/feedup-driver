//
//  ProfileVC.swift
//  FeedUp Eater
//
//  Created by Variance on 22/09/21.
//

import UIKit

class ProfileVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblRatting: UILabel!
    @IBOutlet weak var lblOnlineTime: UILabel!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var lblOrders: UILabel!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Do your stuf here
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

    // MARK: - Button Action Method --
    @IBAction func btnSetting(_ sender: Any)
    {
        let VC: SettingVC = Utilities.viewController(name: "SettingVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! SettingVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let VC: NotificationListVC = Utilities.viewController(name: "NotificationListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! NotificationListVC
        VC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnInviteFriends(_ sender: Any)
    {
        let textToShare = "Check out my app"

        if let myWebsite = URL(string: AppStoreURL) {//Enter link to your app here
            let objectsToShare = [textToShare, myWebsite, #imageLiteral(resourceName: "Logo")] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

            //Excluded Activities
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            //

            activityVC.popoverPresentationController?.sourceView = sender as? UIView
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSecurity(_ sender: Any)
    {
        
    }

    @IBAction func btnAboutUs(_ sender: Any)
    {
    }
    
   
    
    @IBAction func btnLogOut(_ sender: Any)
    {
        LogOutPopUp()
    }
    @IBAction func btnTermsAndCondation(_ sender: Any)
    {
        
    }
    
    
    //MARK:- popup --
    func LogOutPopUp()
    {
        let vc = Utilities.viewController(name: "CustomePopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomePopUpVC
        vc.popup = CustomPopUp.LogOut
        
        self.tabBarController?.tabBar.isHidden = true
        vc.completionHandler = {
            (type: Int) -> () in
            
            if type == 1
            {
                // perform cancel action
                self.tabBarController?.tabBar.isHidden = false
            }
            else
            {
                AppInstance.gotoLoginScreen(transition: false)
            }
            vc.dismiss(animated: true, completion: nil)
            
        }
        self.present(vc, animated: true, completion: nil)
    }

}
