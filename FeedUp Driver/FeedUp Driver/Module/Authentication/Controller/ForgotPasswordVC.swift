//
//  ForgotPasswordVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var txtEmailPhoneNumber: UITextField!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarUIView?.backgroundColor = BGColor
    }
    
    //MARK:- Button Action Method --
    @IBAction func btnSend(_ sender: Any)
    {
        let VC: OTPVC = Utilities.viewController(name: "OTPVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! OTPVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    //MARK: - Validation -
    func validation(isPhone: Bool = false) -> Bool
    {
        if isPhone
        {
            if self.txtEmailPhoneNumber.text?.trim() == ""
            {
                showMessage(message: ENTER_PHONE)
                return false
            }
            else if !Utilities.isValidPhone(phone: self.txtEmailPhoneNumber.text!.trim())
            {
                showMessage(message: INVALID_PHONE)
                return false
            }
        }
        else
        {
            if self.txtEmailPhoneNumber.text?.trim() == ""
            {
                showMessage(message: ENTER_EMAIL)
                return false
            }
            else if !Utilities.isValidEmail(testStr: self.txtEmailPhoneNumber.text!.trim())
            {
                showMessage(message: INVALID_EMAIL)
                return false
            }
        }
        return true
    }

}
//MARK:- UITextField Delegate --
extension ForgotPasswordVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
}
