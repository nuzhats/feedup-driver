//
//  LoginVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit


class LoginVC: UIViewController {

    //MARK:- View Life Cycle
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var btnHideShowPassword: UIButton!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetUpView()
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        UIApplication.shared.statusBarUIView?.backgroundColor = themeColor
    }
    
    //MARK:- SetUp View
    func SetUpView()
    {
        lblSignUp.underline()
    }

    
    //MARK:- Buttion Action Method --
    @IBAction func btnSignIn(_ sender: Any)
    {
        AppInstance.gotoHome(transition: false)
    }
    
    @IBAction func btnSignUp(_ sender: Any)
    {
        let VC: SignUpVC = Utilities.viewController(name: "SignUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! SignUpVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func BtnForgotPassword(_ sender: Any)
    {
        let VC: ForgotPasswordVC = Utilities.viewController(name: "ForgotPasswordVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! ForgotPasswordVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnHideShowPassword(_ sender: Any)
    {
        if btnHideShowPassword.isSelected
        {
            txtPassword.isSecureTextEntry = true
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Show"), for: .normal)
            btnHideShowPassword.isSelected = false
        }
        else
        {
            txtPassword.isSecureTextEntry = false
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
            btnHideShowPassword.isSelected = true
        }
    }
    
    @IBAction func btnLoginWithGoogle(_ sender: Any)
    {
        GIDSignIn.sharedInstance()?.delegate = self
                GIDSignIn.sharedInstance()?.presentingViewController = self
                GIDSignIn.sharedInstance()?.signIn()

    }
    
    @IBAction func btnLoginWithTwiter(_ sender: Any)
    {
        
                    
                

    }
    
    @IBAction func btnLoginWithFacebook(_ sender: Any)
    {
        
        Facebook.LogInWithFacebook(viewController: self, handler: { (response) in
            
            print(response.email)
            print(response.userName)
            print(response.firstName)
            print(response.lastName)
            print(response.gender)
            print(response.profic_pic)
            print(response)
                    
            //Social api call
    
        }) { (error) in
            if error != nil
            {
                showMessage(message: error!.localizedDescription)
            }
            
        }
        
        
    }
    
    //MARK: - Validation -
    func validation() -> Bool
    {
        if self.txtEmail.text?.trim() == ""
        {
            showMessage(message: ENTER_EMAIL)
            return false
        }
        else if !Utilities.isValidEmail(testStr: self.txtEmail.text!.trim())
        {
            showMessage(message: INVALID_EMAIL)
            return false
        }
        else if self.txtPassword.text?.trim() == ""
        {
            showMessage(message: ENTER_PASSWORD)
            return false
        }
        else if !Utilities.isValidPassword(testStr: self.txtPassword.text!.trim(), length: 6)
        {
            showMessage(message: INVALID_PASSWORD)
            return false
        }
        return true
    }
}
//MARK:- UITextField Delegate --
extension LoginVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword
        {
            //Login Api Call
            self.view.endEditing(true)
        }
        else
        {
            self.view.endEditing(true)
        }
        return true
    }
}
//MARK:- Goole SignIn Deleget --
extension LoginVC: GIDSignInDelegate
{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          print("The user has not signed in before or they have since signed out.")
        } else {
          print("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
        _ = user.userID                  // For client-side use only!
        
        _ = user.authentication.idToken // Safe to send to the server
      let fullName = user.profile.name ?? ""
        _ = user.profile.givenName
        _ = user.profile.familyName
      let email = user.profile.email ?? ""
      // ...
        print(fullName)
    }
}
