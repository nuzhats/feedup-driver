//
//  CreatePasswordVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 22/09/21.
//

import UIKit

class CreatePasswordVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfrimPassword: UITextField!
    @IBOutlet weak var btnHideShowPassword: UIButton!
    @IBOutlet weak var btnHideShowConfirmPassword: UIButton!
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK:- Button Action Methods --
    @IBAction func btnSave(_ sender: Any)
    {
    }
    
    @IBAction func btnHideShowPassword(_ sender: Any)
    {
        if btnHideShowPassword.isSelected
        {
            txtPassword.isSecureTextEntry = true
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Show"), for: .normal)
            btnHideShowPassword.isSelected = false
        }
        else
        {
            txtPassword.isSecureTextEntry = false
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
            btnHideShowPassword.isSelected = true
        }
    }
    
    @IBAction func btnHideShowConfirmPassword(_ sender: Any)
    {
        if btnHideShowConfirmPassword.isSelected
        {
            txtConfrimPassword.isSecureTextEntry = true
            btnHideShowConfirmPassword.setImage(#imageLiteral(resourceName: "Show"), for: .normal)
            btnHideShowConfirmPassword.isSelected = false
        }
        else
        {
            txtConfrimPassword.isSecureTextEntry = false
            btnHideShowConfirmPassword.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
            btnHideShowConfirmPassword.isSelected = true
        }
    }
    
    //MARK: - VALIDATION -
    func validation() -> Bool
    {
        if self.txtPassword.text?.trim() == ""
        {
            showMessage(message: ENTER_PASSWORD)
            return false
        }
        else if !Utilities.isValidPassword(testStr: self.txtPassword.text!.trim(), length: 6)
        {
            showMessage(message: INVALID_PASSWORD)
            return false
        }
        else if self.txtConfrimPassword.text?.trim() == ""
        {
            showMessage(message: ENTER_CONFIRMPASSWORD)
            return false
        }
        else if self.txtPassword.text?.trim() != self.txtConfrimPassword.text?.trim()
        {
            showMessage(message: PASSWORD_NOT_MATCH)
            return false
        }
        return true
    }
    
}
//MARK:- UITextField Delegate --
extension CreatePasswordVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtPassword
        {
            txtConfrimPassword.becomeFirstResponder()
        }
        else if textField == txtConfrimPassword
        {
            //Save Api Call
            self.view.endEditing(true)
        }
        else
        {
            self.view.endEditing(true)
        }
        return true
    }
}
