//
//  OTPVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 22/09/21.
//

import UIKit

class OTPVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var otpView: OTPFieldView!
    @IBOutlet weak var btnResendCode: UIButton!
    
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetUpOtpView()
        
    }
    
    //MARK:- SetUp OTP View --
    func SetUpOtpView()
    {
        btnResendCode.underlineButtonText(textColor: themeColor)
        self.otpView.fieldsCount = 4
        self.otpView.fieldBorderWidth = 0
        self.otpView.defaultBorderColor = UIColor.black
        self.otpView.filledBorderColor = UIColor.black
        self.otpView.cursorColor = UIColor.black
        self.otpView.displayType = .square
        self.otpView.fieldSize = 50
        self.otpView.separatorSpace = 20
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.defaultBackgroundColor = UIColor.white
        //self.otpView.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 1, scale: true)
        self.otpView.initializeUI()
    }
    

    //MARK:- Button Action Methods
    @IBAction func btnVerify(_ sender: Any)
    {
        let VC: CreatePasswordVC = Utilities.viewController(name: "CreatePasswordVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CreatePasswordVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnResendCode(_ sender: Any)
    {
    }
    
}
//MARK:- OTP Field View Delegate Methods --
extension OTPVC: OTPFieldViewDelegate
{
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
    }
}
