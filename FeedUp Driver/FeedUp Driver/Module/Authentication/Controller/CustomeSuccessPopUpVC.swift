//
//  CustomeSuccessPopUpVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import UIKit

class CustomeSuccessPopUpVC: UIViewController {
    
    //MARK:- Outlets --
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet weak var imgSuccessImage: UIImageView!
    @IBOutlet weak var btnSuccess: UIButton!
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet var btnClose: UIButton!
    
    
    //MARK:- Varibles --
    var completionHandler: ((_ type: Int) -> ())?
    var popup = popUp.AccountCreate

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPopupData()
        viewHolder.animateViewWith(direction: .top, offset: 200.0)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupPopupData()
    {
        lblTitle.text = popup.title
        lblMsg.text = popup.msg
        imgSuccessImage.image = popup.image
        btnClose.isHidden = popup.isShowCloseButton ? false : true
        btnSuccess.setTitle(popup.btnTitle, for: .normal)
    }
    
    //MARK:- Button Action Method --
    @IBAction func btnSuccess(_ sender: Any)
    {
        if completionHandler != nil
        {
            self.completionHandler!(1)
        }
    }
    @IBAction func btnClose(_ sender: Any)
    {
        if completionHandler != nil
        {
            self.completionHandler!(0)
        }
    }
    
}
