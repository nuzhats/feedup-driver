//
//  SignUpVC.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import UIKit
import Photos

class SignUpVC: UIViewController {

    //MARK:- Outlets --
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfrimPassword: UITextField!
    @IBOutlet weak var btnHideShowPassword: UIButton!
    @IBOutlet weak var btnHideShowConfirmPassword: UIButton!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var txtDriversLicence: UITextField!
    @IBOutlet var txtVehicleDetails: UITextField!
    @IBOutlet var txtVehicleInsurance: UITextField!
    @IBOutlet var txtVehicleInspection: UITextField!
    @IBOutlet var txtProofOfWorkEligibility: UITextField!
    
    //MARK:- Varibles --
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    var imgDriversLicence = UIImage()
    var imgVehicleInsurance = UIImage()
    var imgProofofWorkEligibility = UIImage()
    var isDriversLicence: Bool = false
    var isVehicleInsurance: Bool = false
    var isProofofWorkEligibility: Bool = false
    
    //MARK:- View Life Cycle --
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetUpView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        UIApplication.shared.statusBarUIView?.backgroundColor = BGColor
    }
    
    //MARK:- SetUp View
    func SetUpView()
    {
        lblLogin.underline()
    }
    
    //MARK:- Select Image --
    func SelectImage()
    {
        let alert = UIAlertController(title: Select_Image, message: nil, preferredStyle: .actionSheet)
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        alert.addAction(UIAlertAction(title: Take_Pic, style: .default, handler: { _ in
            switch self.cameraAuthorizationStatus
            {
            case .denied:
                print("Camera permission")
            case .authorized:
                self.openCamera()
            case .restricted:
                print("Camera permission")
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    if granted
                    {
                        //print("Granted access to \(cameraMediaType)")
                        self.openCamera()
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            print("Camera permission")
                        }
                        // print("Denied access to \(cameraMediaType)")
                        
                    }
                }
            @unknown default:
                print("Error")
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: Choose_from_Gallery, style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: BTN_Cancel, style: .cancel, handler: { _ in
            self.isProofofWorkEligibility = false
            self.isVehicleInsurance = false
            self.isDriversLicence = false
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- Open camera --
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            DispatchQueue.main.async {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        else
        {
            print("Camera permission")
        }
    }
    
    //MARK:- Open gallary
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
        {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            print("Camera permission")
        }
    }

    //MARK:- Button Action Method --
    @IBAction func btnSignUp(_ sender: Any)
    {
        SignUpSuccess()
    }
    
    @IBAction func btnGotoLogin(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHideShowPassword(_ sender: Any)
    {
        if btnHideShowPassword.isSelected
        {
            txtPassword.isSecureTextEntry = true
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Show"), for: .normal)
            btnHideShowPassword.isSelected = false
        }
        else
        {
            txtPassword.isSecureTextEntry = false
            btnHideShowPassword.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
            btnHideShowPassword.isSelected = true
        }
    }
    
    @IBAction func btnHideShowConfirmPassword(_ sender: Any)
    {
        if btnHideShowConfirmPassword.isSelected
        {
            txtConfrimPassword.isSecureTextEntry = true
            btnHideShowConfirmPassword.setImage(#imageLiteral(resourceName: "Show"), for: .normal)
            btnHideShowConfirmPassword.isSelected = false
        }
        else
        {
            txtConfrimPassword.isSecureTextEntry = false
            btnHideShowConfirmPassword.setImage(#imageLiteral(resourceName: "Hide"), for: .normal)
            btnHideShowConfirmPassword.isSelected = true
        }
    }
    @IBAction func btnAddProofOfWorkEligibility(_ sender: Any)
    {
        isProofofWorkEligibility = true
        SelectImage()
    }
    
    @IBAction func btnAddVehicleInsurance(_ sender: Any)
    {
        isVehicleInsurance = true
        SelectImage()
    }
    
    @IBAction func btnAddDriversLicence(_ sender: Any)
    {
        isDriversLicence = true
        SelectImage()
    }
    
    //MARK:- popup --
    func SignUpSuccess()
    {
        let vc = Utilities.viewController(name: "CustomeSuccessPopUpVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! CustomeSuccessPopUpVC
        vc.popup = popUp.AccountCreate
        
        
        vc.completionHandler = {
            (type: Int) -> () in
            
            
            if type == 0//Close button
            {
                // perform close action
                vc.dismiss(animated: true, completion: nil)
            }
            else
            {
                //Perform Main action
                vc.dismiss(animated: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
                
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- Validation -
    func validation() -> Bool
    {
        if self.txtName.text?.trim() == ""
        {
            showMessage(message: ENTER_NAME)
            return false
        }
        else if self.txtPhoneNumber.text?.trim() == ""
        {
            showMessage(message: ENTER_PHONE)
            return false
        }
        else if !Utilities.isValidPhone(phone: self.txtPhoneNumber.text!.trim())
        {
            showMessage(message: INVALID_PHONE)
            return false
        }
        else if self.txtEmail.text?.trim() == ""
        {
            showMessage(message: ENTER_EMAIL)
            return false
        }
        else if !Utilities.isValidEmail(testStr: self.txtEmail.text!.trim())
        {
            showMessage(message: INVALID_EMAIL)
            return false
        }
        else if self.txtAddress.text?.trim() == ""
        {
            showMessage(message: ENTER_Address)
            return false
        }
        else if imgDriversLicence.size.height == 0
        {
            showMessage(message: SELECT_Driver_Licence)
            return false
        }
        else if self.txtVehicleDetails.text?.trim() == ""
        {
            showMessage(message: ENTER_Vehicle_Details)
            return false
        }
        else if imgVehicleInsurance.size.height == 0
        {
            showMessage(message: SELECT_Vehicle_Insurance)
            return false
        }
        else if self.txtVehicleInspection.text?.trim() == ""
        {
            showMessage(message: ENTER_Vehicle_Inspection)
            return false
        }
        else if imgProofofWorkEligibility.size.height == 0
        {
            showMessage(message: SELECT_Work_Eligibility)
            return false
        }
        else if self.txtPassword.text?.trim() == ""
        {
            showMessage(message: ENTER_PASSWORD)
            return false
        }
        else if !Utilities.isValidPassword(testStr: self.txtPassword.text!.trim(), length: 6)
        {
            showMessage(message: INVALID_PASSWORD)
            return false
        }
        else if self.txtConfrimPassword.text?.trim() == ""
        {
            showMessage(message: ENTER_CONFIRMPASSWORD)
            return false
        }
        else if self.txtPassword.text?.trim() != self.txtConfrimPassword.text?.trim()
        {
            showMessage(message: PASSWORD_NOT_MATCH)
            return false
        }
        return true
    }
}
//MARK:- UITextField Delegate --
extension SignUpVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtName
        {
            txtPhoneNumber.becomeFirstResponder()
        }
        else if textField == txtPhoneNumber
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword
        {
            txtConfrimPassword.becomeFirstResponder()
        }
        else if textField == txtConfrimPassword
        {
            txtAddress.becomeFirstResponder()
        }
        else if textField == txtAddress
        {
            txtVehicleDetails.becomeFirstResponder()
        }
        else if textField == txtVehicleDetails
        {
            txtVehicleInspection.becomeFirstResponder()
        }
        else if textField == txtVehicleInspection
        {
            self.view.endEditing(true)
        }
        else
        {
            self.view.endEditing(true)
        }
        return true
    }
}
//MARK: - Image Picker Delegate Methods -
extension SignUpVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        var pickedImage = UIImage()
        if let img1 = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        {
            pickedImage = img1
            
        }
        else if let img1 = info[.originalImage] as? UIImage
        {
            pickedImage = img1
        }
        if isProofofWorkEligibility
        {
            imgProofofWorkEligibility = pickedImage
        }
        else if isVehicleInsurance
        {
            imgVehicleInsurance = pickedImage
        }
        else
        {
            imgDriversLicence = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
}
