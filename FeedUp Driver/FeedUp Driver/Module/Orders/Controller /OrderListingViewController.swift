//
//  OrderListingViewController.swift
//  FeedUp Driver
//
//  Created by Macbook on 30/09/21.
//

import UIKit

class OrderListingViewController: UIViewController {

    //MARK: - OUTLETS -
    
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var tableViewOrders: UITableView!
    
    //MARK: - VARIABLES -
    
    //MARK: LIFE CYCLE METHODS -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    //MARK: - BUTTON ACTIONS -
    
    @IBAction func btnApply(_ sender: Any) {
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let resultVC: NotificationListVC = Utilities.viewController(name: "NotificationListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! NotificationListVC
        
        self.navigationController?.pushViewController(resultVC, animated: true)
    }
    
    //MARK: - API CALLING -

}

//MARK: - TABLEVIEW DELEGATE/DATASOURCE -

extension OrderListingViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: OrderTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell", for: indexPath) as? OrderTableViewCell)!
        cell.selectionStyle = .none
        
        return cell
    }
}
