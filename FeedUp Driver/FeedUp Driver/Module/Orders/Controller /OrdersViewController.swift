//
//  OrdersViewController.swift
//  FeedUp Driver
//
//  Created by Macbook on 24/09/21.
//

import UIKit

class OrdersViewController: UIViewController {

    //MARK: - OUTLETS -
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var lblDropOffAddress: UILabel!
    
    @IBOutlet weak var lblEaterName: UILabel!
    @IBOutlet weak var imgEater: UIImageView!
    @IBOutlet weak var lblFeederName: UILabel!
    @IBOutlet weak var imgFeeder: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblOrderDetails: UILabel!
    @IBOutlet weak var lblPickupAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDriver: UILabel!
    
    
    
    //MARK: - VARIABLES -
    
    //MARK: - LIFE CYCLE METHODS -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK:- BUTTON ACTIONS -
    
    @IBAction func btnMEssageFeeder(_ sender: Any) {
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let resultVC: NotificationListVC = Utilities.viewController(name: "NotificationListVC", onStoryBoared: StoryBoaredNames.profile.rawValue) as! NotificationListVC
        
        self.navigationController?.pushViewController(resultVC, animated: true)
    }
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCallFeeder(_ sender: Any) {
    }
    
    @IBAction func btnCallEater(_ sender: Any) {
    }
    
    @IBAction func btnMessageEater(_ sender: Any) {
    }
}
