//
//  AppDelegate.swift
//  FeedUp Driver
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import GoogleMaps
import Firebase
import GoogleSignIn
import FacebookCore

var AppInstance: AppDelegate!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    var device_token = UUID().uuidString
    let locationManager = CLLocationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AppInstance = self
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = BTN_DONE
        LocationPermission()
        GMSServices.provideAPIKey(GoogleMapKey)
        /*window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }*/
        //Configure Firebase
        FirebaseApp.configure()

        
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID

        //Facebook
        ApplicationDelegate.shared.application(
                    application,
                    didFinishLaunchingWithOptions: launchOptions
                )
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(
            _ app: UIApplication,
            open url: URL,
            options: [UIApplication.OpenURLOptionsKey : Any] = [:]
        ) -> Bool {

            ApplicationDelegate.shared.application(
                app,
                open: url,
                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplication.OpenURLOptionsKey.annotation]
            )
        }
    
    
    //MARK:- Get Loccation Permission --
    func LocationPermission()
    {
        if CLLocationManager.authorizationStatus() == .notDetermined
        {
            locationManager.requestAlwaysAuthorization()
        }
        else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
        {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            //locationManager.allowsBackgroundLocationUpdates=true
            locationManager.delegate = self
            
        }
    }

    func gotoHome(transition :Bool, tabIndex:Int = 0)
    {
        let tabbarVC = Utilities.viewController(name: "tabBarVC", onStoryBoared: StoryBoaredNames.tabbar.rawValue) as? UITabBarController
        let transitionOption = transition ? UIView.AnimationOptions.transitionFlipFromLeft : UIView.AnimationOptions.showHideTransitionViews
        gotoViewController(viewController: tabbarVC!, transition: transitionOption)
    }

    func gotoLoginScreen(transition: Bool)
    {
        var _navigation = UINavigationController()
        
        let VC: LoginVC = Utilities.viewController(name: "LoginVC", onStoryBoared: StoryBoaredNames.authentication.rawValue) as! LoginVC
        
        _navigation = UINavigationController(rootViewController: VC)
        _navigation.navigationBar.backgroundColor = .clear
        
        let transitionOption = transition ? UIView.AnimationOptions.transitionFlipFromLeft : UIView.AnimationOptions.transitionFlipFromLeft
        
        gotoViewController(viewController: _navigation, transition: transitionOption)
    }
    func gotoViewController(viewController :UIViewController, transition: UIView.AnimationOptions)
    {
        if transition != UIView.AnimationOptions.showHideTransitionViews
        {
            UIView.transition(with: self.window!, duration: 0.5, options: transition, animations: {
                self.window?.rootViewController = viewController
            }) { (finished) in
                
            }
        }
        else
        {
            window?.rootViewController = viewController
        }
    }
}

