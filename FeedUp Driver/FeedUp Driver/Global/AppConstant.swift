//
//  AppConstant.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit

var AppName = "FeedUp Eater"
var AppStoreURL = "http://itunes.apple.com/app/id"
var PlayStoreURL = ""
var SupportEmail = ""
var passwordLength = 4

let GoogleMapKey = "AIzaSyDrcDGBNq7OszkcxINQJG32h3MlO0tTS0k"

//MARK: - Socket details -
enum SocketEnvironment: String {
    case Production = ""
    case Dev = "http://35.154.14.28:3002"
    case Local = "http://192.168.1.112:3002"
}

//Socket Environment set up
let socketEnvironment : SocketEnvironment = .Dev
var SOCKET_URL: String = socketEnvironment.rawValue

////////////////
///User default object
var DEFAULT = UserDefaults.standard


let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String

struct ScreenSize
{
    static let SCREEN_SIZE = UIScreen.main.bounds.size
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

///Storyboared name list
enum StoryBoaredNames : String
{
    case main = "Main"
    case authentication = "Authentication"
    case tabbar = "TabBar"
    case profile = "Profile"
    case transaction = "Transaction"
    case home = "Home"
    case order = "Orders"
}

let popUp = AppPopUpSuccess()
struct AppPopUpSuccess
{
    var AccountCreate = (title : "", msg: Account_Create, image : #imageLiteral(resourceName: "Delivered"), btnTitle : BTN_Continue, isShowCloseButton: false)
    
    var PICK_UP = (title : "", msg: CONFIRM_PICKUP, image : #imageLiteral(resourceName: "Pickup"), btnTitle : GO_TO_ICKUP, isShowCloseButton: true)
    
    var COMPLETE_DROPOFF = (title : ORDERE_DELIVERED, msg: ORDER_DELIVERED_MSG, image : #imageLiteral(resourceName: "Delivered"), btnTitle : BTN_DONE, isShowCloseButton: true)
}
let CustomPopUp = CustomAppPopUpDetails()
struct CustomAppPopUpDetails
{
    var LogOut = (title : "", msg : MSG_Logout,isSingle : false, mainTitle : BTN_Logout, otherTitle : BTN_Cancel)
    
    var DeleteAccount = (title : Alert, msg : MSG_Delete_Account,isSingle : false, mainTitle : BTN_Delete, otherTitle : BTN_Cancel)
    
    var Delete = (title : Alert, msg : MSG_Delete_Confirmation,isSingle : false, mainTitle : BTN_Yes, otherTitle : BTN_No)
    
    
}

