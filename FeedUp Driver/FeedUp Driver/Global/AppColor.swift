//
//  AppColor.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit

let themeColor = UIColor().hexStringToUIColor(hex: "354958")
let ShadowColor = UIColor.init(red: 154.0/255, green: 154.0/255, blue: 154.0/255, alpha: 0.5)
let customRedColor = UIColor().hexStringToUIColor(hex: "FF0000")
let BGColor = UIColor().hexStringToUIColor(hex: "F7F7F7")
