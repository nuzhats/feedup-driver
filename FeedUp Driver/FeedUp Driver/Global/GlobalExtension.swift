//
//  GlobalExtension.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit

extension UITextField {
    open override func awakeFromNib() {
        //self.autocapitalizationType = .sentences
        if #available(iOS 11.0, *) {
            //            self.navigationBar.prefersLargeTitles = true
            //            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }
    

}

@IBDesignable extension UIView {
    
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.20
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2.0
    }
    
    /* The corner radius. Defaults to 0.*/
    @IBInspectable var cornerRadiusV: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    /* The color of the shadow. Defaults to opaque black. Colors created
     * from patterns are currently NOT supported. Animatable. */
    
    @IBInspectable var _shadowColor: UIColor?
        {
        set {
            layer.shadowColor = newValue!.cgColor
        }
        get {
            if let color = layer.shadowColor {
                return UIColor.init(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    
    /* The opacity of the shadow. Defaults to 0. Specifying a value outside the
     * [0,1] range will give undefined results. Animatable. */
    @IBInspectable var shadowOpacity: Float
        {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    
    /* The shadow offset. Defaults to (0, -3). Animatable. */
    @IBInspectable var shadowOffset: CGPoint
        {
        set {
            layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
        }
        get {
            return CGPoint(x: layer.shadowOffset.width, y:layer.shadowOffset.height)
        }
    }
    
    /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
    @IBInspectable var shadowRadius: CGFloat
        {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
    
    
    /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
    @IBInspectable var borderWidth: CGFloat
        {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
    @IBInspectable var borderColor: UIColor
        {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
    }
    
    @IBInspectable var animateWithType: Int
        {
        set(value) {
            if let type = AnimationDirectionType(rawValue: value) {
                animate(withType: [AnimationType.from(direction: type, offSet: 20.0)])
            }
        }
        get {
            return self.animateWithType
        }
    }
    
    func animateViewWith(direction : AnimationDirectionType , offset : CGFloat) {
        animate(withType: [AnimationType.from(direction: direction, offSet: offset)])
    }
    
    
    @IBInspectable var Radius: CGFloat {
        get{return self.layer.cornerRadius}
        set(newValue)
        {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    @IBInspectable var TopLeftRightCorner: CGFloat {
        get{return self.layer.cornerRadius}
        set(newValue)
        {
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            self.clipsToBounds = true
        }
    }
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
      layer.masksToBounds = false
      layer.shadowColor = color.cgColor
      layer.shadowOpacity = opacity
      layer.shadowOffset = offSet
      layer.shadowRadius = radius

      layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
      layer.shouldRasterize = true
      layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}


extension UINavigationController {
    open override func awakeFromNib() {
        if #available(iOS 11.0, *) {
            //            self.navigationBar.prefersLargeTitles = true
            //            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePopGestureRecognizer?.delegate = nil
        //
        //        if #available(iOS 11.0, *) {
        //            UINavigationBar.appearance().largeTitleTextAttributes =
        //                [NSAttributedStringKey.foregroundColor: kColorBlack,
        //                 NSAttributedStringKey.font: UIFont.applyBold(fontSize: 34)]
        //        } else {
        //            // Fallback on earlier versions
        //        }
        
    }
    
    func popToViewController<T: UIViewController>(withType type: T.Type) -> Bool
    {
        var isPoped: Bool = false
        for viewController in self.viewControllers
        {
            if viewController is T
            {
                self.popToViewController(viewController, animated: true)
                isPoped = true
                break
            }
        }
        return isPoped
    }
}

extension UINavigationBar
{
    open override func awakeFromNib()
    {
        setupNavigation()
    }
    
    func setupNavigation()
    {
        self.barTintColor = UIColor.black
        //navigation bar color
        self.backgroundColor = .white//UIColor(red: 53.0/255.0, green: 95.0/255.0, blue: 81.0/255.1, alpha: 1)
        
        self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        //        self.shadowImage = UIImage()
        self.isOpaque = true
        self.tintColor = UIColor.black
        
        self.titleTextAttributes = [ NSAttributedString.Key.font : UIFont.applyBold(fontSize : 20) , NSAttributedString.Key.foregroundColor : UIColor.black]
        
        
        // code for adjust shadow of inbuilt navigationbar
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.10).cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 0
        setBackgroundColor(UIColor.white)
        shouldRemoveShadow(true)
    }
    
    func shouldRemoveShadow(_ value: Bool)
    {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
    
    func setBackgroundColor(_ color: UIColor)
    {
        if self.accessibilityIdentifier != "detail"
        {
            self.barTintColor = color
            self.backgroundColor = color
            UIApplication.shared.statusBarUIView?.backgroundColor = color
        }
        else
        {
            self.barTintColor = .clear
            self.backgroundColor = .clear
            UIApplication.shared.statusBarUIView?.backgroundColor = .clear
        }
    }
}

extension UIApplication {
var statusBarUIView: UIView? {
    if #available(iOS 13.0, *) {
        let tag = 38482
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        /*if let statusBar = keyWindow?.viewWithTag(tag) {
            return statusBar
        } else {*/
            guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
            let statusBarView = UIView(frame: statusBarFrame)
            statusBarView.tag = tag
            keyWindow?.addSubview(statusBarView)
            return statusBarView
       // }
    } else if responds(to: Selector(("statusBar"))) {
        return value(forKey: "statusBar") as? UIView
    } else {
        return nil
    }
  }
}


extension UIBarButtonItem
{
    open override func awakeFromNib()
    {
    
        //adjust navigation title color
        self.setTitleTextAttributes([ NSAttributedString.Key.font : UIFont.applyRegular(fontSize: 16) , NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        //help to change bar button color
        self.tintColor = UIColor.black
        
    }
}

extension UIFont
{
    //Assign you font style here
    class func applyRegular(fontSize : CGFloat) -> UIFont
    {
        return UIFont.init(name: "SF-Pro-Display-Regular" , size: 16.0) ?? UIFont.systemFont(ofSize:fontSize)
        //return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyBold(fontSize : CGFloat) -> UIFont
    {
        return UIFont.init(name: "SF-Pro-Display-Bold" , size: 18.0) ?? UIFont.systemFont(ofSize:fontSize)
        //return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyBoldItalic(fontSize : CGFloat) -> UIFont
    {
        //return UIFont.init(name: "GoogleSans-BoldItalic" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyItalic(fontSize : CGFloat) -> UIFont
    {
        //return UIFont.init(name: "GoogleSans-Italic" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyMeduimItalic(fontSize : CGFloat) -> UIFont
    {
        //return UIFont.init(name: "GoogleSans-MediumItalic" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyMedium(fontSize : CGFloat) -> UIFont
    {
        //return UIFont.init(name: "GoogleSans-Medium" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
    }
}

extension UIView
{
    func showToastWithMessgae(message:String, title:String? = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String)  {
        
    }
    
    func giveShadow()
    {
        self.layer.masksToBounds = false
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.shadowColor = ShadowColor.cgColor//UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.7, height: 0.7)
        self.layer.shadowRadius = 5
        self.layer.cornerRadius = 3
        self.layer.shadowOpacity = 0.5
        //self.layer.shadowPath = shadowPath.cgPath
        
        /*layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 1
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1*/
        
    }
    
    func addViewBorder(borderColor:CGColor,borderWith:CGFloat,borderCornerRadius:CGFloat){
            self.layer.borderWidth = borderWith
            self.layer.borderColor = borderColor
            self.layer.cornerRadius = borderCornerRadius

    }
    
}

/**
 Returns an UIImage with a specified background color.
 - parameter color: The color of the background
 */
extension UIImage
{
    convenience init(withBackground color: UIColor)
    {
        let rect: CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size);
        let context:CGContext = UIGraphicsGetCurrentContext()!;
        context.setFillColor(color.cgColor);
        context.fill(rect)
        
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        self.init(ciImage: CIImage(image: image)!)
    }
    func imageWithColor(color: UIColor) -> UIImage?
    {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIScrollView
{
    var currentPage: Int
    {
        return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)+1
    }
}

extension String
{
    
    var first: String {
        return String(prefix(1))
    }
    var last: String {
        return String(suffix(1))
    }
    
    var uppercaseFirst: String {
        return first.uppercased() + String(dropFirst())
    }
    
    /**
     :name:    trim
     */
    public func trim() -> String
    {
        return trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    func sizeForWidth(width: CGFloat, font: UIFont) -> CGSize
    {
        let attr = [NSAttributedString.Key.font: font]
        let height = NSString(string: self).boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options:.usesLineFragmentOrigin, attributes: attr, context: nil).height
        return CGSize(width: width, height: ceil(height))
    }
    
    var lastPathComponent: String {
        
        get
        {
            return (self as NSString).lastPathComponent
        }
        
        
    }
    
    var pathExtension: String
    {
        
        get
        {
            
            return (self as NSString).pathExtension
        }
    }
    
    var stringByDeletingLastPathComponent: String {
        
        get
        {
            
            return (self as NSString).deletingLastPathComponent
        }
    }
    
    var stringByDeletingPathExtension: String {
        
        get
        {
            
            return (self as NSString).deletingPathExtension
        }
    }
    
    var pathComponents: [String] {
        get
        {
            return (self as NSString).pathComponents
        }
    }
    
    func stringByAppendingPathComponent(path: String) -> String
    {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String?
    {
        let nsSt = self as NSString
        return nsSt.appendingPathExtension(ext)
    }
    
    func containsWhiteSpace() -> Bool {
        
        // check if there's a range for a whitespace
        let range = self.rangeOfCharacter(from: .whitespaces)
        
        // returns false when there's no range for whitespace
        if let _ = range {
            return true
        } else {
            return false
        }
    }
    
    ///Added by Shahabuddin - 12-12-2018
    func trimMobile() -> String
    {
        return self.replacingOccurrences(of: " ", with: "")
    }
    func setFormattedMobile() -> String
    {
        return self.chunk(n: 3)
            .map{ String($0) }.joined(separator: " ")
    }
    func setDefaultVal() -> String
    {
        return self.trim() == "" ? "- -" : self
    }
    func removeDefaultVal() -> String
    {
        return self.trim() == "..." ? "" : self
    }
    func setOrderIdVal() -> String
    {
        return self.trim() == "" ? "..." : "VT\(self)"
    }
    func setQuantityVal() -> String
    {
        return self.trim() == "" ? "..." : "Qty: \(self)"
    }
    func setPriceVal() -> String
    {
        return self.trim() == "" ? "..." : "KSh \(self)"
    }
    func setPriceFormattedVal() -> String
    {
        let largeNumber = self
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return self.trim() == "" ? "..." : "\(numberFormatter.string(for: largeNumber) ?? "0")"
        //return self.trim() == "" ? "..." : "KSh \(self)"
    }
    func setDiscountVal() -> String
    {
        return self.trim() == "" ? "..." : "\(self)% Discount"
    }
    func setNoDescriptionVal() -> String
    {
        return self.trim() == "" ? "No Description" : self
    }
    
    func setStrikeText() -> NSAttributedString
    {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
        return attributeString
    }
    func setHtmlText() -> NSAttributedString
    {
        return try! NSAttributedString(data: self.data(using: String.Encoding.unicode) ?? Data(), options: [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html], documentAttributes: nil)
    }
    func trimZeroAtStart() -> String
    {
        let scanner = Scanner(string: self)
        let zeros = CharacterSet(charactersIn: "0")
        scanner.scanCharacters(from: zeros, into: nil)
        let result = (NSString(string: self)).substring(from: scanner.scanLocation)
        print("\(self) reduced to \(String(describing: result))")
        
        return result
    }
    var toLocale: Locale
    {
        return Locale(identifier: self)
    }
    public func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
  
    

    
    var integer: Int {
        return Int(self) ?? 0
    }
    
    var secondFromString : Int{
        let components: Array = self.components(separatedBy: ":")
        let hours = components[0].integer
        let minutes = components[1].integer
        let seconds = components[2].integer
        return Int((hours * 60 * 60) + (minutes * 60) + seconds)
    }
    static func get24HoursTime(dateStr:String,NotificationReminderTime: Double) -> (Int, Int){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: dateStr)
        var Houres = 0
        var Minute = 0
        if date != nil
        {
            let date1 = date!.addingTimeInterval(TimeInterval(-NotificationReminderTime * 60.0))

            dateFormatter.dateFormat = "HH:mm"
            _ = dateFormatter.string(from: date1)
            Houres = Calendar.current.component(.hour, from: date1)
            Minute = Calendar.current.component(.minute, from: date1)
        }
        
        return (Houres, Minute)
    }
    
    static func get24HoursTimeWith10Minute(dateStr:String,NotificationReminderTime: Double) -> (Int, Int){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: dateStr)
        var Houres = 0
        var Minute = 0
        if date != nil
        {
            let date1 = date!.addingTimeInterval(TimeInterval(-NotificationReminderTime * 60.0))
            Houres = Calendar.current.component(.hour, from: date1)
            Minute = Calendar.current.component(.minute, from: date1)

            dateFormatter.dateFormat = "HH:mm"
            _ = dateFormatter.string(from: date1)
        }
        
        return (Houres, Minute)
    }
    
    static func EventSyncDate(dateStr:String, DateFormate:String) -> (Int, Int, Int){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormate.replacingOccurrences(of: "mm", with: "MM")
        let date = dateFormatter.date(from: dateStr)
        var Year = 0
        var Month = 0
        var Day = 0
        if date != nil
        {
            Year = Calendar.current.component(.year, from: date!)
            Month = Calendar.current.component(.month, from: date!)
            Day = Calendar.current.component(.day, from: date!)
        }
        return (Year, Month, Day)
    }
    
    static func CustomeFilterDateConvert(dateStr:String, DateFormate:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormate.replacingOccurrences(of: "mm", with: "MM")
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strConvertedDate = dateFormatter.string(from: date ?? Date())
        return strConvertedDate
    }
    static func GetDateForEventSync(date:Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: date)
        return date
    }
}

extension Collection
{
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}
/// Array Index Extension which is returns Index of object
extension Array where Array.Element: AnyObject
{
    func index(ofElement element: Element) -> Int?
    {
        for (currentIndex, currentElement) in self.enumerated()
        {
            if currentElement === element
            {
                return currentIndex
            }
        }
        return nil
    }
}

extension Numeric
{
    func currency(numberStyle: NumberFormatter.Style = NumberFormatter.Style.currency, locale: String, groupingSeparator: String? = nil, decimalSeparator: String? = nil) -> String?
    {
        return currency(numberStyle: numberStyle, locale: locale.toLocale, groupingSeparator: groupingSeparator, decimalSeparator: decimalSeparator)
    }
    func currency(numberStyle: NumberFormatter.Style = NumberFormatter.Style.currency, locale: Locale = Locale.current, groupingSeparator: String? = nil, decimalSeparator: String? = nil) -> String?
    {
        if let num = self as? NSNumber {
            let formater = NumberFormatter()
            formater.locale = locale
            formater.numberStyle = numberStyle
            var formatedSting = formater.string(from: num)
            if let separator = groupingSeparator, let localeValue = locale.groupingSeparator {
                formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
            }
            if let separator = decimalSeparator, let localeValue = locale.decimalSeparator  {
                formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
            }
            return formatedSting
        }
        return nil
    }
}

extension Double
{
    func setPriceFormatted() -> String
    {
        let largeNumber = self
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return self.description.trim() == "" ? "0" : numberFormatter.string(for: largeNumber) ?? "0"
    }
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double
    {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension UISearchBar {
    
    func change(textFont : UIFont?) {
        
        for view : UIView in (self.subviews[0]).subviews {
            
            if let textField = view as? UITextField {
                textField.font = textFont
            }
        }
    }
    
}

extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
}


extension String {
    
    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }
    
    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
}


var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

extension String {
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension UILabel {
    func setHTMLFromString(text: String) {
        let modifiedFont = NSString(format:"<span style=\"color:\(self.textColor ?? UIColor.darkGray);font-family: \(self.font!.fontName); font-size: \(self.font!.pointSize)\">%@</span>" as NSString, text)
        
        
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
}
extension URL {
    static func createFolder(folderName: String) -> URL? {
        let fileManager = FileManager.default
        // Get document directory for device, this should succeed
        if let documentDirectory = fileManager.urls(for: .documentDirectory,
                                                    in: .userDomainMask).first {
            // Construct a URL with desired folder name
            let folderURL = documentDirectory.appendingPathComponent(folderName)
            // If folder URL does not exist, create it
            if !fileManager.fileExists(atPath: folderURL.path) {
                do {
                    // Attempt to create folder
                    try fileManager.createDirectory(atPath: folderURL.path,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
                } catch {
                    // Creation failed. Print error & return nil
                    print(error.localizedDescription)
                    return nil
                }
            }
            // Folder either exists, or was created. Return URL
            return folderURL
        }
        // Will only be called if document directory not found
        return nil
    }
    
}

extension String
{
    func validateUrl () -> Bool
    {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }
}
extension String {
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
}
extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}
extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}


extension UIView {
   func createDottedLine(width: CGFloat, color: CGColor) {
      let caShapeLayer = CAShapeLayer()
      caShapeLayer.strokeColor = color
      caShapeLayer.lineWidth = width
      caShapeLayer.lineDashPattern = [5,5]
      let cgPath = CGMutablePath()
      let cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: self.frame.width - 40, y: 0)]
      cgPath.addLines(between: cgPoint)
      caShapeLayer.path = cgPath
      layer.addSublayer(caShapeLayer)
   }
}
extension NSAttributedString {

    /// Returns a new instance of NSAttributedString with same contents and attributes with strike through added.
     /// - Parameter style: value for style you wish to assign to the text.
     /// - Returns: a new instance of NSAttributedString with given strike through.
     func withStrikeThrough(_ style: Int = 1) -> NSAttributedString {
         let attributedString = NSMutableAttributedString(attributedString: self)
         attributedString.addAttribute(.strikethroughStyle,
                                       value: style,
                                       range: NSRange(location: 0, length: string.count))
         return NSAttributedString(attributedString: attributedString)
     }
}
extension String {
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"

        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)

        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }

        return (self as NSString).substring(with: result.range)
    }
}
extension UIView {
    func dropShadowOnView(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 1
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func setCornerRadius(corners : UIRectCorner, radius : CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

//Global Back Button --
extension UIViewController
{
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGlobalBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: NSRange(location: 0, length: textString.count))
            self.attributedText = attributedString
        }
    }
}
extension UIButton {
    func underlineButtonText(textColor: UIColor) {
    guard let title = title(for: .normal) else { return }

    let titleString = NSMutableAttributedString(string: title)
    titleString.addAttribute(
      .underlineStyle,
      value: NSUnderlineStyle.single.rawValue,
      range: NSRange(location: 0, length: title.count)
    )
    titleString.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: NSRange(location:0,length:title.count))
    setAttributedTitle(titleString, for: .normal)
  }
}

