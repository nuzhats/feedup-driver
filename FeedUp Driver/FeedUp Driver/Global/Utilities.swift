//
//  Utilities.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit
import SwiftyJSON
import AVKit


class Utilities
{
    class func viewController(name:String, onStoryBoared storyboared: String) -> UIViewController
    {
        let sb = UIStoryboard(name: storyboared, bundle: nil)
        return sb.instantiateViewController(withIdentifier: name) as UIViewController
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func isValidPhone(phone: String) -> Bool {
        
        let phoneRegex = "^[0-9]{6,14}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
    
    class func isValidPassword(testStr:String, length: Int) -> Bool
    {
        if testStr.count<length
        {
            return false
        }
        return true
    }
    
    class func validateUrl (urlString: String) -> Bool
    {
        /*let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)*/
        if urlString.contains(".")
        {
            let firstString = urlString.components(separatedBy: ".").first
            let lastString = urlString.components(separatedBy: ".").last
            if firstString != "" && lastString != ""
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
    }
    
    class func resizeImage(image: UIImage) -> UIImage {
        let size = image.size
        let targetSize = CGSize(width: 500, height: 500)
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    class func LocalCountryCode() -> String
    {
        let currentLocale = Locale.current as NSLocale
        let strCode = currentLocale.object(forKey: .countryCode)
        return strCode as? String ?? ""
    }

    func generateThumbnailImage(filePath :String) -> UIImage
    {
        let asset = AVURLAsset(url: URL(fileURLWithPath: filePath), options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        do
        {
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            // !! check the error before proceeding
            let uiImage = UIImage(cgImage: cgImage)
            return uiImage
            
        }catch
        {
            print("Error in generating thumbnail")
            return UIImage()
        }
    }
    
    //MARK: - Generate JSON String -
    class func getJSONstring(value : Any) ->  NSString
    {
        do {
            let temp = try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            return NSString(data: temp, encoding: String.Encoding.utf8.rawValue)!
        }
        catch let error as NSError
        {
            print(error)
            return NSString()
        }
    }
   
    class func convertCurrencyToDouble(currency:String)->Double
    {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // USA: Locale(identifier: "en_US")
        formatter.numberStyle = .decimal
        let number = formatter.number(from: currency ?? "0")
        return number!.doubleValue
    }
    
    class func yesterday() -> Date {

       var dateComponents = DateComponents()
       dateComponents.setValue(-1, for: .day) // -1 day

       let now = Date() // Current date
       let yesterday = Calendar.current.date(byAdding: dateComponents, to: now) // Add the DateComponents

       return yesterday!
    }
    
    class func tomorrow() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.setValue(1, for: .day); // +1 day
        
        let now = Date() // Current date
        let tomorrow = Calendar.current.date(byAdding: dateComponents, to: now)  // Add the DateComponents
        
        return tomorrow!
    }
}
