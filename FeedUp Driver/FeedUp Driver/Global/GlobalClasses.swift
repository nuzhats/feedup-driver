//
//  GlobalClasses.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit

class TextFielCustom: UITextField {
    
    @IBInspectable var leftImage : UIImage? {
        didSet {
            if let image = leftImage{
                leftViewMode = .always
                let imageView = UIImageView(frame: CGRect(x: 12.5, y: 0, width: 25, height: 25))
                imageView.contentMode = .scaleAspectFit
                imageView.image = image
                imageView.tintColor = tintColor
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 50, height: 25))
                view.addSubview(imageView)
                leftView = view
            }else {
                leftViewMode = .never
            }
        }
    }
    /*
     override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
     return bounds.inset(by: padding)
     }
     
     override func editingRect(forBounds bounds: CGRect) -> CGRect {
     return bounds.inset(by: padding)
     }
     
     override func textRect(forBounds bounds: CGRect) -> CGRect {
     return bounds.inset(by: padding)
     }
     */
}

class TextFielCustomWithImage: UITextField {
    
    //var padding = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
    
    @IBInspectable var leftImage : UIImage? {
        didSet {
            if let image = leftImage{
                leftViewMode = .always
                let imageView = UIImageView(frame: CGRect(x: 12.5, y: 0, width: 24, height: 25))
                imageView.contentMode = .scaleAspectFit
                imageView.image = image
                imageView.tintColor = tintColor
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 50, height: 25))
                view.addSubview(imageView)
                leftView = view
            }else {
                leftViewMode = .never
            }
        }
    }
    /*
     override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
     return bounds.inset(by: padding)
     }
     
     override func editingRect(forBounds bounds: CGRect) -> CGRect {
     return bounds.inset(by: padding)
     }
     
     override func textRect(forBounds bounds: CGRect) -> CGRect {
     return bounds.inset(by: padding)
     }
     */
}

class BackgroundImage: UIImageView {
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
    }
}

func getConvertedDate(dateStr:String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    dateFormatter.timeZone = NSTimeZone.local
    let date = dateFormatter.date(from: dateStr)
    let strDateFormate = UserDefaults.standard.value(forKey: "date_format") as? String ?? "mm-dd-yyyy"
    dateFormatter.dateFormat = strDateFormate.replacingOccurrences(of: "mm", with: "MM")
    return  dateFormatter.string(from: date!)
}
/*class ThemeButton : UIButton {
    
    override func awakeFromNib() {
        self.applyStyle(buttonFont: UIFont.applyRegular(fontSize: 14),
                        textColor: UIColor.black,
                        cornerRadius: 0.0,
                        backgroundColor: UIColor.green,
                        borderColor: nil,
                        borderWidth: nil,
                        state: .normal)
    }
}*/


func changeBoldStringChartecterUsingRange(wholeString:String,changeFontStr:String) -> NSAttributedString
{
    
    let attributedString = NSMutableAttributedString(string: wholeString, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)])
    let boldFontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16.0)]
    
    attributedString.addAttributes(boldFontAttribute, range: NSMakeRange(0, changeFontStr.count))
    
    return attributedString
}


@IBDesignable class TextViewWithPlaceholder: UITextView {
    
    override var text: String! { // Ensures that the placeholder text is never returned as the field's text
        get {
            if showingPlaceholder {
                return "" // When showing the placeholder, there's no real text to return
            } else { return super.text }
        }
        set { super.text = newValue }
    }
    @IBInspectable var placeholderText: String = ""
    @IBInspectable var placeholderTextColor: UIColor = UIColor(red: 0.78, green: 0.78, blue: 0.80, alpha: 1.0) // Standard iOS placeholder color (#C7C7CD). See https://stackoverflow.com/questions/31057746/whats-the-default-color-for-placeholder-text-in-uitextfield
    private var showingPlaceholder: Bool = true // Keeps track of whether the field is currently showing a placeholder
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        if text.isEmpty {
            showPlaceholderText() // Load up the placeholder text when first appearing, but not if coming back to a view where text was already entered
        }
    }
    
    override func becomeFirstResponder() -> Bool {
        // If the current text is the placeholder, remove it
        if showingPlaceholder {
            text = nil
            textColor = nil // Put the text back to the default, unmodified color
            showingPlaceholder = false
        }
        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        // If there's no text, put the placeholder back
        if text.isEmpty {
            showPlaceholderText()
        }
        return super.resignFirstResponder()
    }
    func textViewDidChange(_ textView: UITextView)
    {
        if UserDefaults.standard.bool(forKey: "rtl_theme")
        {
            textView.textAlignment = .right
        }
        else
        {
            textView.textAlignment = .left
        }
        
    }
    private func showPlaceholderText() {
        showingPlaceholder = true
        textColor = placeholderTextColor
        text = placeholderText
    }
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

