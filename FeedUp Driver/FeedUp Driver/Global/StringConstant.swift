//
//  StringConstant.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation


var MSG_NO_INTERNET = "Network connection error!"
var MSG_SOMETHING_WRONG = "Something went wrong!"//////

var Account_Create = "Your Application has been successfully submitted, You will receive a confirmation email from FeedUp."
var BTN_Continue = "Continue"

var ENTER_EMAIL = "the email address cannot be blank"
var ENTER_PASSWORD = "the password cannot be blank"
var ENTER_NAME = "the name cannot be blank"
var ENTER_CONFIRMPASSWORD = "the confirm password cannot be blank"
var ENTER_PHONE = "Please enter phone number"

var INVALID_EMAIL = "Please enter valid email"
var INVALID_PASSWORD = "Please enter valid password"
var PASSWORD_NOT_MATCH = "the password did not match"
var INVALID_PHONE = "Please enter valid phone number"


var MSG_Logout = "Are you sure you want to Logout?"
var BTN_Logout = "Logout"
var BTN_Cancel = "Cancel"

var Alert = "Alert"
var MSG_Delete_Account = "Are you sure you want to Delete Account?"
var BTN_Delete = "Delete"

var MSG_Delete_Confirmation = "Are you sure you want to Delete?"
var BTN_Yes = "Yes"
var BTN_No = "No"
var BTN_DONE = "Done"










var CONFIRM_PICKUP = "Confirm your PICKUP"
var GO_TO_ICKUP = "Go to Pickup"
var PICKUP = "Pickup"
var DROPOFF = "Dropoff"



var Select_Image = "Select Image"
var Take_Pic = "Take Pic"
var Choose_from_Gallery = "Choose from Gallery"

var ORDERE_DELIVERED = "ORDER DELIVERED"
var ORDER_DELIVERED_MSG = "You have successfully completed the delivery and received your earnings $13."

var ENTER_Address = "the address cannot be blank"
var SELECT_Driver_Licence = "upload driver licen"
var ENTER_Vehicle_Details = "provide vehicle details"
var SELECT_Vehicle_Insurance = "upload vehicle insurance"
var ENTER_Vehicle_Inspection = "the Vehicle Inspection cannot be blank"
var SELECT_Work_Eligibility = "upload Proof of work eligibility"
