//
//  GlobalFunctions.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import UIKit
import TTGSnackbar
import CoreLocation
import NVActivityIndicatorView
import Alamofire


func ShowIndicator()
{
    let activityData = ActivityData(size: CGSize(width: 50, height: 50), message: "", messageFont: nil, messageSpacing: nil, type: .ballSpinFadeLoader, color: themeColor, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor.black.withAlphaComponent(0.1), textColor: nil)
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
}

func HideIndicator()
{
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
}

func showMessage(message: String)
{
    if message.lowercased() == "unauthenticated" {
        showMessage(message: MSG_SOMETHING_WRONG +  "\nplease login again")
        //kCurrentUser.logOut()
        //kCurrentUser.saveToDefault()
        //AppInstance.gotoLoginScreen(transition: true)
        return
    }
    
    let snackbar: TTGSnackbar = TTGSnackbar.init(message: message, duration: .middle)
    
    // Change the content padding inset
    snackbar.contentInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
    
    // Change margin
    snackbar.leftMargin = 8
    snackbar.rightMargin = 8
    
    // Change message text font and color
    snackbar.messageTextColor = .white//UIColor(red:0.22, green:0.29, blue:0.36, alpha:1.00)
    snackbar.messageTextFont = UIFont.boldSystemFont(ofSize: 18)
    snackbar.messageTextAlign = .natural
    
    // Change snackbar background color
    snackbar.backgroundColor = .darkGray//.red//themeColor//UIColor(red:0.30, green:0.72, blue:0.53, alpha:1.00)
    
    // Change animation duration
    snackbar.animationDuration = 0.5
    
    // Animation type
    snackbar.animationType = .slideFromTopBackToTop
    
    snackbar.show()
}
//---------//------------//-------------//

func calculateDistance(coordinate₀: CLLocation, coordinate₁: CLLocation) -> String
{
    /*let coordinate₀ = CLLocation(latitude: 5.0, longitude: 5.0)
     let coordinate₁ = CLLocation(latitude: 5.0, longitude: 3.0)*/
    
    let distanceInMeters = coordinate₀.distance(from: coordinate₁)
    let distanceInMiles = Int(distanceInMeters) / 1609
    
    return "\(distanceInMiles) miles"
}


func saveDefault(withObject: AnyObject ,forkey:String)
{
    let deft = UserDefaults.standard
    deft.set(withObject, forKey: forkey)
    deft.synchronize()
}

func getDefault(forkey:String) -> AnyObject?
{
    let deft = UserDefaults.standard
    return (deft.value(forKey: forkey) as AnyObject?) ?? nil
}

// MARK:- base 64
func encodeToBase64String(image: UIImage?) -> String? {
    return image!.pngData()?.base64EncodedString(options: .lineLength64Characters)
}

func decodeBase64ToImage(strEncodeData: String?) -> UIImage? {
    let data = Data(base64Encoded: strEncodeData ?? "", options: .ignoreUnknownCharacters)
    if let aData = data {
        return UIImage(data: aData)
    }
    return nil
}

func getDocumentsDirectory() -> URL? {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths.first
}

func deleteFileAtPath(url : String){
    if url.isEmpty {return}
    
    do {
        try FileManager.default.removeItem(at: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)
        print("File deleted successfully")
    }catch{
        print("1st attempt to delete is failed")
        do {try FileManager.default.removeItem(atPath: url)}catch{ print("2nd attempt to delete is failed")}
    }
}


func getStringFromDictionary(dict:NSDictionary) -> String{
    
    let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: [])
    let decoded = String(data: jsonData, encoding: .utf8)!
    
    return decoded
    
}

//MARK: - Check is back space pressed or not -
func isBackSpace(string: String)->Bool
{
    if let char = string.cString(using: String.Encoding.utf8) {
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            print("Backspace was pressed")
            return true
        }
    }
    return false
}



func getAddressFromLatLong(Latitude: String, Longitude: String)->String
{
    if CheckNetworkConnection()
    {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(Latitude)")!
        //21.228124
        let lon: Double = Double("\(Longitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        var addressString : String = ""
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0
                {
                    let pm = placemarks![0]
                    
                    if pm.subLocality != nil
                    {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil
                    {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil
                    {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil
                    {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil
                    {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                    
                }
                
                
        })
        return addressString
    }
    else
    {
        HideIndicator()
        showMessage(message: MSG_NO_INTERNET)
        return ""
    }
}
func CheckNetworkConnection() -> Bool{
    let rechabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")

    rechabilityManager?.startListening()

    if let r = rechabilityManager
    {
        switch r.isReachable
        {
        case true:
            return true
        case false:
            return false
        }
    }
    return false
}


enum AddressResponse
{
    case success(address: String, Dic: CLPlacemark)
    case error(error: String)
}

func AddressFromLatLong(Latitude: Double, Longitude: Double, response: @escaping (AddressResponse) -> Void)//->String
{
    if CheckNetworkConnection()
    {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat = Latitude//: Double = Double("\(Latitude)")!
        //21.228124
        let lon = Longitude//: Double = Double("\(Longitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        var addressString : String = ""
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    
                    response(.error(error: "reverse geodcode fail: \(error!.localizedDescription)"))
                }
                if placemarks != nil
                {
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0
                    {
                        let pm = placemarks![0]
                        
                        if pm.subLocality != nil
                        {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil
                        {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil
                        {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil
                        {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil
                        {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        
                        print(addressString)
                        
                        response(.success(address: addressString, Dic: pm))
                    }
                }
                
                
                
        })
        //return addressString
    }
    else
    {
        HideIndicator()
        showMessage(message: MSG_NO_INTERNET)
        //return ""
    }
}

