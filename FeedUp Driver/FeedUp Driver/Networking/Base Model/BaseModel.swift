//
//  BaseModel.swift
//  FeedUp Eater
//
//  Created by Jayesh Godhaniya on 21/09/21.
//

import Foundation
import SwiftyJSON

class BaseModel: NSObject {
    var identifier :String?
    var pushSetting: NSMutableDictionary?
    
    override init() {
        super.init()
    }
    
    init(info: JSON)
    {
        super.init()
        updateUserInfo(info)
    }
    
    func updateUserInfo(_ info: JSON)
    {
        
    }
}
